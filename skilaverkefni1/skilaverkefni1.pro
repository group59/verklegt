TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    dataaccess.cpp \
    datarepository.cpp \
    domainactions.cpp \
    uiconsole.cpp \
    person.cpp

include(deployment.pri)
qtcAddDeployment()

HEADERS += \
    dataaccess.h \
    uiconsole.h \
    domainactions.h \
    datarepository.h \
    person.h

OTHER_FILES += \
    user_menustart.txt\
    people.txt
