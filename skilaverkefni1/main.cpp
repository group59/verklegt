#include "uiconsole.h"
#include "datarepository.h"

using namespace std;

// The only thing that the main function does is to create an instance
// of the UIConsole class and then starts the UI
int main()
{
    UIConsole ui = UIConsole();
    ui.start();

    return 0;
}
