#ifndef DATAREPOSITORY_H
#define DATAREPOSITORY_H

#include "dataaccess.h"
#include "person.h"

// Repo keeps the vector as private and the member functions give access to it
//includes add and save options int its public part which are called in the domain actions

class DataRepository
{
    private:
        vector<Person> v; // the vector is kept private within the repo
    public:
        DataRepository();
        int getSize(); // returns the size of the vector
        bool isEmpty(); // returns T or F, depending if the vector is empty or not
        Person getPerson(int i); // returns the Person in the vector at position i
        void add(Person p);
        void save(); // saves the vector by calling the save function in DataAccess
        vector<Person> getVector(); // returns a copy of the vector
};

#endif // DATAREPOSITORY_H
