#include "datarepository.h"

//the list of all the actions performed in the DataRepository

DataRepository::DataRepository() {
    DataAccess data = DataAccess();
    v = data.load();
}

int DataRepository::getSize() {
    return v.size();
}

bool DataRepository::isEmpty() {
    return v.empty();
}

Person DataRepository::getPerson(int i) {
    return v[i];
}

void DataRepository::add(Person p) {
    v.push_back(p);
}

void DataRepository::save() {
    DataAccess data = DataAccess();
    data.save(v);
}

vector<Person> DataRepository::getVector() {
    return v;
}
