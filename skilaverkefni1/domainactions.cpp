#include "domainactions.h"

DomainActions::DomainActions()
{
    drepo = DataRepository();
}

void DomainActions::add(Person p)
{
    drepo.add(p); //calls to DataRepository
}

// The command options list given to the user,
// action functions are being called here depending on the command obtained from the user
void DomainActions::viewList() {

    cout << endl << "Sort the list:"
         << endl << "1: By name."
         << endl << "2: By gender."
         << endl << "3: By year of birth."
         << endl << "4: By year of death."
         << endl << "If any other command is entered, the list will be output as it is."
         << endl << endl << "> ";
    string command;
    cin.ignore();
    getline(cin, command);
    cout << endl;

    if(command == "1") {
        listByNameA();
    }
    else if(command == "2") {
        listByGenderA();
    }
    else if(command == "3") {
        listByYearOfBirthA();
    }
    else if(command == "4") {
        listByYearOfDeathA();
    }
    else {
        listAsIs();
    }
}

//if list is empty, then add to the list by "pushing back" into it,
//if list is not empty, sort it in ascending order
void DomainActions::listByNameA() {
    if(!drepo.isEmpty()) {
        list<Person> temp = list<Person>();
        for(int i = 0; i < drepo.getSize(); i++) {
            temp.push_back(drepo.getPerson(i));
        }
        temp.sort(CompareNameA());
        print(temp);
    }
}

void DomainActions::listByGenderA() {
    if(!drepo.isEmpty()) {
        list<Person> temp = list<Person>();
        for(int i = 0; i < drepo.getSize(); i++) {
            temp.push_back(drepo.getPerson(i));
        }
        temp.sort(CompareGenderA());
        print(temp);
    }
}

void DomainActions::listByYearOfBirthA() {
    if(!drepo.isEmpty()) {
        list<Person> temp = list<Person>();
        for(int i = 0; i < drepo.getSize(); i++) {
            temp.push_back(drepo.getPerson(i));
        }
        temp.sort(CompareYOB_A());
        print(temp);
    }
}

void DomainActions::listByYearOfDeathA() {
    if(!drepo.isEmpty()) {
        list<Person> temp = list<Person>();
        for(int i = 0; i < drepo.getSize(); i++) {
            temp.push_back(drepo.getPerson(i));
        }
        temp.sort(CompareYOD_A());
        print(temp);
    }
}

void DomainActions::listAsIs() {
    if(!drepo.isEmpty()) {
        vector<Person> temp = drepo.getVector();
        print(temp);
    }
}

void DomainActions::print(list<Person> temp) {
    if(!temp.empty()) {
        cout.width(1); cout << "";
        cout.width(32); cout << left << "Name";
        cout.width(10); cout << left << "Gender";
        cout.width(6); cout << left << "Born";
        cout.width(8); cout << right << "Died" << endl;

        for(int j = 0; j < 58; j++) {
            cout << "-";
        }
        cout << endl;

        for(list<Person>::iterator iter = temp.begin(); iter != temp.end(); iter++) {
            cout.width(1); cout << "";
            cout.width(32); cout << left << iter->Name;
            cout.width(10); cout << left << iter->Gender;
            cout.width(6); cout << left << iter->YearOfBirth;
            cout.width(8); cout << right << iter->YearOfDeath << endl;
        }
    }
}

void DomainActions::print(vector<Person> temp) {
    if(!temp.empty()) {
        cout.width(1); cout << "";
        cout.width(32); cout << left << "Name";
        cout.width(10); cout << left << "Gender";
        cout.width(6); cout << left << "Born";
        cout.width(8); cout << right << "Died" << endl;

        for(int j = 0; j < 58; j++)
        {
            cout << "-";
        }
        cout << endl;

        for(unsigned int i = 0; i < temp.size(); i++) {
            cout.width(1); cout << "";
            cout.width(32); cout << left << temp[i].Name;
            cout.width(10); cout << left << temp[i].Gender;
            cout.width(6); cout << left << temp[i].YearOfBirth;
            cout.width(8); cout << right << temp[i].YearOfDeath << endl;
        }
    }
}

void DomainActions::search() {
    if(!drepo.isEmpty()) {
        string toSearch;
        cin >> toSearch;
        list<Person> temp = list<Person>();
        for(int i = 0; i < drepo.getSize(); i++) {
            string tmpPrefix = drepo.getPerson(i).Name.substr(0, toSearch.size());
            if(toSearch == tmpPrefix) {
                temp.push_back(drepo.getPerson(i));
            }
        }
        if(!temp.empty()) {
            cout << endl << "Found " << temp.size();
            if(temp.size() == 1) {
                cout << " result";
            }
            else {
                cout << " results";
            }
            cout << " containing '" << toSearch << "'." << endl;
            if(temp.size() > 1) {
                cout << endl << "How shall I sort them?" << endl;
                cout << "1: By name." << endl << "2: By gender." << endl
                     << "3: By year born." << endl << "4: By year died." << endl << "> ";
                int toSortBy;
                //cin.ignore();
                //getline(cin, toSortBy);
                // doesn't work with int, need to change more code to make it work with getline
                cin >> toSortBy;
                cout << endl;
                switch(toSortBy) {
                case 1:
                    temp.sort(CompareNameA());
                    print(temp);
                    break;
                case 2:
                    temp.sort(CompareGenderA());
                    print(temp);
                    break;
                case 3:
                    temp.sort(CompareYOB_A());
                    print(temp);
                    break;
                case 4:
                    temp.sort(CompareYOD_A());
                    print(temp);
                    break;
                default:
                    cout << "Unknown command. You have to pick from 1-4" << endl;
                    break;
                }
            }
            else {
                print(temp);
            }
        }
        else {
            cout << endl << "There was no name found that starts with '" << toSearch << "'" << endl;
        }
    }
    else {
        cout << endl << "There's no data to search in." << endl;
    }
}

void DomainActions::save() {
    drepo.save();
}
