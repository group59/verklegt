#ifndef PERSON_H
#define PERSON_H

#include <string>
using namespace std;

//class that defines each person that is already on the list
//or that is going to be added to the list
class Person {
    public:
        Person();
        string Name;
        string Gender;
        string YearOfBirth;
        string YearOfDeath;
};

#endif // PERSON_H
