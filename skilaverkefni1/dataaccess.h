#ifndef DATAACCESS_H
#define DATAACCESS_H

#include "person.h"
#include <vector>
#include <iostream>
#include <fstream>
#include <string>

class DataAccess
{
    public:
        // saves the vector to a csv file for safekeeping
        void save(vector<Person> v);

        // used for opening and loading the csv file into a vector for the repository
        // outputs a error message if the file can't be opened
        vector<Person> load();
};

#endif // DATAACCESS_H
