#ifndef DOMAINACTIONS_H
#define DOMAINACTIONS_H

#include <list>
#include <vector>
#include "person.h"
#include "datarepository.h"

// Domain Layer - the class contains all the actions that need to be done to the DB
class DomainActions
{
private:
    DataRepository drepo;
    void listByNameA();                 //function fetches database and sorts by name in ascending order
    void listByGenderA();               //function fetches database and sorts by gender in ascending order
    void listByYearOfBirthA();          //function fetches database and sorts by year of birth in ascending order
    void listByYearOfDeathA();          //function fetches database and sorts by year of death in ascending order
    void listAsIs();                    //this function fetches the database and performs no sorting
    void print(list<Person> temp);      //print-to-screen function for list types
    void print(vector<Person> temp);    //print-to-screen function for vector types
public:
    DomainActions();       // constructor initiates an instance of Repo
    void viewList();       // called upon with the list command. Askes how to sort the list
                           // and then prints it to the screen
    void add(Person p);    // used by UI layer, sends Person p onwards to the Data layer
    void search();         // searches for a name by prefix and outputs to the screen. Askes
                           // how to sort the list if there're more than 1 entries found
    void save();
};

// Below are structs that make overwriting the () operator possible
// One for each way that the DB is sorted
struct CompareNameA                // the A at the end stands for Ascending
{                                  // Descending order to be added later
    inline bool operator() (const Person& first, const Person& second) {
        return first.Name < second.Name;
    }
};

struct CompareGenderA              // the A at the end stands for Ascending
{                                  // Descending order to be added later
    inline bool operator() (const Person& first, const Person& second) {
        return first.Gender < second.Gender;
    }
};

struct CompareYOB_A                // the A at the end stands for Ascending
{                                  // Descending order to be added later
    inline bool operator() (const Person& first, const Person& second) {
        return first.YearOfBirth < second.YearOfBirth;
    }
};

struct CompareYOD_A                // the A at the end stands for Ascending
{                                  // Descending order to be added later
    inline bool operator() (const Person& first, const Person& second) {
        return first.YearOfDeath < second.YearOfDeath;
    }
};

#endif // DOMAINACTIONS_H
