Alan Mathison Turing;Male;1912;1954
Augusta Ada Byron;Female;1815;1852
Charles Babbage;Male;1791;1871
Grace Hopper;Female;1906;1992
David A. Huffman;Male;1925;1999
John Vincent Atanasoff;Male;1903;1995
George Boole;Male;1815;1864
Edsger Wybe Dijkstra;Male;1930;2002
Annie J. Easley;Female;1933;2011
Douglas Engelbart;Male;1925;2013