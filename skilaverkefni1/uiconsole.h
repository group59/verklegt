#ifndef UICONSOLE_H
#define UICONSOLE_H
#include "person.h"
#include "domainactions.h"
#include <string>

class UIConsole
{
private:
    DomainActions dactions;
public:
    UIConsole();
    void start();     // starts the UI, calls the welcome, help and repl functions
    bool execute_command(string cmd);
    void help();      // prints out the help menu
    void repl();
    void welcome();   // displays the awesome welcome message
    void getInfo();   // if the command entered is add, this function gets info about the
                      // Person to be added and sends it onwards to the Domain Layer
};

#endif // UICONSOLE_H
