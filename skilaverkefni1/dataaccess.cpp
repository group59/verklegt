#include "dataaccess.h"

//saves the data file to the given address in case if someone
//was added to the list

void DataAccess::save(vector<Person> v)
{
    ofstream file ("people.txt");
    if(file.is_open()) {
        if(!v.empty()) {
            for(unsigned int i = 0; i < v.size()-1; i++) {  // only goes to size-1 so the endline char isn't added to the last line
                file << v[i].Name << ";" << v[i].Gender << ";" << v[i].YearOfBirth
                << ";" << v[i].YearOfDeath << endl;
            }
            file << v[v.size()-1].Name << ";" << v[v.size()-1].Gender << ";"
            << v[v.size()-1].YearOfBirth << ";" << v[v.size()-1].YearOfDeath; // adds the last line without the endline char
            file.close();
        }
    }
    else
    {
        cout << "Database could not be saved. :(" << endl; // or outputs a error message if the file can't be opened
    }
}

//used for opening the text file from the given address
//while the file it open it is able to output the data it has in it
//and it makes it possible to add the data to the vector by "pushing back"
//if the file is not open, the output is the informing message of no open file.

vector<Person> DataAccess::load()
{
    ifstream file ("people.txt");
    vector<Person> v = vector<Person>();
    if(file.is_open()) {
        while(!file.eof()) {
            Person p;
            getline(file, p.Name, ';');
            getline(file, p.Gender, ';');
            getline(file, p.YearOfBirth, ';');
            getline(file, p.YearOfDeath);
            v.push_back(p);
        }
        file.close();
    }
    else {
        cout << "The file didn't open. Most likely because there's no file to open." << endl;
    }
    return v; // returns the vector of Persons
}
