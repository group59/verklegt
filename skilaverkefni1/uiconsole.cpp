#include "uiconsole.h"

UIConsole::UIConsole() {
    dactions = DomainActions();
}

void UIConsole::start()
{
    welcome();
    cout << endl;
    help();
    repl();
}

void UIConsole::help() {
    cout << endl << "The following commands are available:" << endl
         << "help:              Prints this help menu." << endl
         << "add:               Adds a person to the database." << endl
         << "save:              Saves the current database to file." << endl
         << "list:              List all the people in the database in a certain order." << endl
         << "search 'name':     View all contacts whose name starts with 'name'" << endl
         << "quit/exit:         Saves changes and quits the Database Manager." << endl;
}

void UIConsole::getInfo() {
    Person p = Person();
    cin.ignore();
    cout << "Enter the person's name: ";
    getline(cin, p.Name);
    cout << "Enter the person's gender: ";
    cin >> p.Gender;
    cout << "Enter the person's year of birth: ";
    cin >> p.YearOfBirth;
    cout << "Enter the person's year of death: ";
    cin.ignore();
    getline(cin, p.YearOfDeath); //allows to send in empty string or a string with spaces

    dactions.add(p);
}

bool UIConsole::execute_command(string cmd) {
    if(cmd == "help") {
        help();
    }
    else if(cmd == "list") {
        dactions.viewList();
    }
    else if(cmd == "add") {
        cout << endl;
        getInfo();
    }
    else if(cmd == "search") {
        dactions.search();
    }
    else if(cmd == "save") {
        dactions.save();
        cout << "The database has been successfully saved to file." << endl;
    }
    else if(cmd == "quit" || cmd == "exit") {
        dactions.save();
        return false;
    }
    else {
        cout << "Unknown command '" << cmd << "'" << endl;
    }
    return true;
}

void UIConsole::repl() {
    string cmd;
    do {
        cout << endl << "Database Manager:> ";
    } while(cin >> cmd && execute_command(cmd));   // magic :) keeps the UI going while the
                                                   // input isn't either quit or exit
    cout << endl << "Exiting. Goodbye :(" << endl << endl;
}

void UIConsole::welcome() {
    ifstream menu ("user_menustart.txt");
    cout << menu.rdbuf();
}
