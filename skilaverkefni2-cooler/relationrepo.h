#ifndef RELATIONREPO_H
#define RELATIONREPO_H

#include "datasqlaccess.h"
#include "person.h"
#include "computer.h"
#include "relation.h"

// handles interaction with the relations table in database
// relation table is the many-to-many relation table

class RelationRepo
{
public:
    RelationRepo();
    bool tableExists();
    void add(int personID, int computerID);
    list<Relation> viewList();
    void findIDs(string person, string computer);
private:
    DataSqlAccess relationData;
    QString tableName;
    QSqlDatabase relationDB;
    void createTable();
};

#endif // RELATIONREPO_H
