TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle

QT += core sql

SOURCES += main.cpp \
    uiconsole.cpp \
    person.cpp \
    computer.cpp \
    personrepo.cpp \
    computerrepo.cpp \
    datasqlaccess.cpp \
    databaseinit.cpp \
    domaincomputeractions.cpp \
    domainpeopleactions.cpp \
    domainrelationactions.cpp \
    utilities.cpp \
    relationrepo.cpp \
    relation.cpp

include(deployment.pri)
qtcAddDeployment()

HEADERS += \
    uiconsole.h \
    person.h \
    computer.h \
    personrepo.h \
    computerrepo.h \
    datasqlaccess.h \
    databaseinit.h \
    domaincomputeractions.h \
    domainpeopleactions.h \
    domainrelationactions.h \
    utilities.h \
    relationrepo.h \
    relation.h \
    variables.h

OTHER_FILES += \
    user_menustart.txt\
    people.txt
