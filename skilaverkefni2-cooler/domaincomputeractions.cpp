#include "domaincomputeractions.h"

DomainComputerActions::DomainComputerActions() {
    crepo = ComputerRepo();
}

//adds new computer and information about it to the database
void DomainComputerActions::add(string name, string s_yearBuilt, string type, bool wb) {
    int yb = 0;
    if(s_yearBuilt != "") {
        yb = atoi(s_yearBuilt.c_str());
    }

    if(validateCompName(name) && validateYear(yb)) {
        Computer c = Computer(name, yb, type, wb);
        crepo.add(c);
    }
    else {
        throw runtime_error("Adding Computer failed. Some input was in the wrong format.");
    }
}

bool DomainComputerActions::validateCompName(string s) {
    return s.length() > 1;
}

// validates that an integer is at least 0 and below 2015
bool DomainComputerActions::validateYear(int year) {
    return year < 2015 && year >= 0;
}

// 1 for Name, 2 for YearBuilt, 3 for Type, 4 for WasBuilt
list<Computer> DomainComputerActions::getListByName(string toSortBy) {
    list<Computer> c = crepo.viewList("1");
    if(toSortBy == "asc") {
        return c;
    }
    else {
        c.reverse();
        return c;
    }
}

list<Computer> DomainComputerActions::getListByYearBuilt(string toSortBy) {
    list<Computer> c = crepo.viewList("2");
    if(toSortBy == "asc") {
        return c;
    }
    else {
        c.reverse();
        return c;
    }
}

list<Computer> DomainComputerActions::getListByType(string toSortBy) {
    list<Computer> c = crepo.viewList("3");
    if(toSortBy == "asc") {
        return c;
    }
    else {
        c.reverse();
        return c;
    }
}

list<Computer> DomainComputerActions::getListByWasBuilt() {
    list<Computer> c = crepo.viewList("4");
    return c;
}

list<Computer> DomainComputerActions::search(string toSearch) {
    list<Computer> c = crepo.search(toSearch);
    return c;
}

bool DomainComputerActions::isRepoEmpty()
{
    return !crepo.tableExists();
}
