#include "databaseinit.h"

// starts a connection to the database, supposed to keep it open until program ends

DatabaseInit::DatabaseInit()
{
}

void DatabaseInit::start()
{
    init.connect();

    UIConsole ui = UIConsole();
    ui.start();

    init.disconnect();
}

