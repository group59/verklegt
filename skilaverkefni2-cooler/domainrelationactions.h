#ifndef DOMAINRELATIONACTIONS_H
#define DOMAINRELATIONACTIONS_H

#include <list>
#include <string>
#include <cstdlib>
#include <stdexcept>
#include "relationrepo.h"
using namespace std;

class DomainRelationActions {
private:
    RelationRepo rrepo;
public:
    DomainRelationActions();
    void add(string first, string second);
    list<Relation> getAllRelations();
};

#endif // DOMAINRELATIONACTIONS_H
