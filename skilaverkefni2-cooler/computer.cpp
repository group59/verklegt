#include "computer.h"

Computer::Computer() {
    Name = "";
    YearBuilt = 0;
    Type = "";
    WasBuilt = 0;
}

Computer::Computer(string name, int yb, string type, bool wb) {
    Name = name;
    YearBuilt = yb;
    Type = type;
    WasBuilt = wb;
}

void Computer::print() {
    cout.width(1); cout << "";
    cout.width(vars.c_name_length); cout << left << Name;
    if(WasBuilt) {
        cout.width(vars.c_yearbuilt_length); cout << left << YearBuilt;
    } else {
        cout.width(vars.c_yearbuilt_length); cout << left << "N/A";
    }
    cout.width(vars.c_type_length); cout << left << Type;
    if(WasBuilt) {
        cout.width(vars.c_wasbuilt_length); cout << right << "Yes" << endl;
    } else {
        cout.width(vars.c_wasbuilt_length); cout << right << "No" << endl;
    }
}
