#ifndef UTILITIES_H
#define UTILITIES_H

#include "variables.h"
#include "person.h"
#include "computer.h"
#include "relation.h"
#include <list>
#include <string>

class Utilities {
    private:
        Variables vars;
    public:
        void print(list<Person> p);          // print-to-screen function for a list of Persons
        void print(list<Computer> c);        // print-to-screen function for a list of Computers
        void print(list<Person> p, list<Computer> c); // print-to-screen function for two lists
        void print(list<Relation> r);        // print-to-screen function for a list of Relations
};

#endif // UTILITIES_H
