#ifndef PERSONREPO_H
#define PERSONREPO_H

#include "datasqlaccess.h"
#include "person.h"

// handles all database interactions regarding Persons or Scientists
class PersonRepo
{
public:
    PersonRepo();
    void add(Person p);
    list<Person> viewList(string toSort);
    list<Person> search(string tofind);
    bool tableExists();
private:
    DataSqlAccess personData;
    QString tableName;
    QSqlDatabase personDB;
    void createTable();
};

#endif // PERSONREPO_H
