#ifndef DATABASEINIT_H
#define DATABASEINIT_H

#include "datasqlaccess.h"
#include "uiconsole.h"

class DatabaseInit
{
public:
    DatabaseInit();
    void start();
private:
    DataSqlAccess init;
};

#endif // DATABASEINIT_H
