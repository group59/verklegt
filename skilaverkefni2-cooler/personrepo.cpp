#include "personrepo.h"

PersonRepo::PersonRepo()
{
    personData = DataSqlAccess();
    tableName = "Scientists";
    personDB = personData.getDatabaseConnection();
}

// checks if a table with tablename exists in the database
bool PersonRepo::tableExists()
{
    if(personDB.tables().contains(tableName))
    {
        return true;
    }
    else
    {
        return false;
    }
}

// creates a table with tablename, if query command fails writes out to qdebug
void PersonRepo::createTable()
{
    QSqlQuery query(personDB);

    if(!query.exec("CREATE TABLE "+tableName+"(ID INTEGER PRIMARY KEY AUTOINCREMENT, Name VARCHAR NOT NULL, Gender VARCHAR NOT NULL, Born INTEGER NOT NULL, Died INTEGER);"))
    {
        qDebug() << query.lastError();
    }
}

// adds a person to the Scientists table in the database
// calls for table creation if it doesn't exist
void PersonRepo::add(Person p)
{

    if(!tableExists())
    {
        createTable();
    }

    QSqlQuery query(personDB);

    query.prepare("INSERT INTO Scientists (Name, Gender, Born, Died)"
                  "VALUES(:name, :gender, :born, :died);");
    query.bindValue(":name", QString::fromStdString(p.Name));
    query.bindValue(":gender", QString::fromStdString(p.Gender));
    query.bindValue(":born", p.YearOfBirth);
    query.bindValue(":died", p.YearOfDeath);

    if(!query.exec())
    {
        qDebug() << query.lastError();
    }

}

// takes in a string to determine what kind of call to table should be done
// then adds results from table into a list, returns that list
list<Person> PersonRepo::viewList(string toSort)
{
    list<Person> persons = list<Person>();

    QSqlQuery query(personDB);

    if(toSort == "1")
    {
        query.exec("SELECT * FROM "+tableName+" ORDER BY Name");
    }
    else if(toSort == "2")
    {
        query.exec("SELECT * FROM Scientists ORDER BY Gender");
    }
    else if(toSort == "3")
    {
        query.exec("SELECT * FROM Scientists ORDER BY Born");
    }
    else if(toSort == "4")
    {
        query.exec("SELECT * FROM Scientists ORDER BY Died");
    }
    else
    {
        query.exec("SELECT * FROM Scientists ORDER BY ID");
    }

    while(query.next())
    {
        Person p = Person();
        p.Name = query.value("Name").toString().toStdString();
        p.Gender = query.value("Gender").toString().toStdString();
        p.YearOfBirth = query.value("Born").toInt();
        p.YearOfDeath = query.value("Died").toInt();

        persons.push_back(p);
    }

    return persons;
}

// searches for a name in the name column in the table and returns a list if a name is found
list<Person> PersonRepo::search(string tofind)
{
    QString tempFind = QString::fromStdString(tofind);

    list<Person> searchPerson = list<Person>();

    QSqlQuery query(personDB);

    query.prepare("SELECT * FROM "+tableName+" s WHERE s.Name LIKE '%"+tempFind+"%'");
    if(!query.exec())
    {
        qDebug() << query.lastError();
    }

    while(query.next())
    {
        Person p = Person();
        p.Name = query.value("Name").toString().toStdString();
        p.Gender = query.value("Gender").toString().toStdString();
        p.YearOfBirth = query.value("Born").toInt();
        p.YearOfDeath = query.value("Died").toInt();

        searchPerson.push_back(p);
    }

    return searchPerson;
}
