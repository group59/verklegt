#ifndef PERSON_H
#define PERSON_H

#include "variables.h"
#include <string>
#include <iostream>
using namespace std;

// A class that defines and keeps info about each person
class Person {
    private:
        Variables vars;
    public:
        Person();
        Person(string name, string gender, int yob, int yod);
        string Name;
        string Gender;
        int YearOfBirth;
        int YearOfDeath;
        void print();
};

#endif // PERSON_H
