#ifndef COMPUTER_H
#define COMPUTER_H

#include "variables.h"
#include <string>
#include <iostream>
using namespace std;

class Computer {
    private:
        Variables vars;
    public:
        Computer();
        Computer(string name, int yb, string type, bool wb);
        string Name;
        string Type;
        bool WasBuilt;
        int YearBuilt;
        string typeToString(int typeNr);
        void print();
};

#endif // COMPUTER_H
