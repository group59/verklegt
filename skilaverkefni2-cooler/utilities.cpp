#include "utilities.h"

void Utilities::print(list<Person> p) {
        cout.width(1); cout << "";
        cout.width(vars.p_name_length); cout << left << "Name:";
        cout.width(vars.p_gender_length); cout << left << "Gender:";
        cout.width(vars.p_yearofbirth_length); cout << left << "Born:";
        cout.width(vars.p_yearofdeath_length); cout << right << "Died:" << endl;

        cout << " ";
        for(int j = 0; j < vars.p_line_length; j++) {
            cout << "-";
        }
        cout << endl;

        for(list<Person>::iterator iter = p.begin(); iter != p.end(); iter++) {
            iter->print();
        }
}

void Utilities::print(list<Computer> c) {
        cout.width(1); cout << "";
        cout.width(vars.c_name_length); cout << left << "Computer name:";
        cout.width(vars.c_yearbuilt_length); cout << left << "Year built:";
        cout.width(vars.c_type_length); cout << left << "Computer type:";
        cout.width(vars.c_wasbuilt_length); cout << right << "Was built:" << endl;

        cout << " ";
        for(int j = 0; j < vars.c_line_length; j++) {
            cout << "-";
        }
        cout << endl;

        for(list<Computer>::iterator iter = c.begin(); iter != c.end(); iter++) {
            iter->print();
        }
}

void Utilities::print(list<Person> p, list<Computer> c) {
        cout << "\n\n";
        print(p);
        cout << "\n\n";
        print(c);
        cout << "\n\n";
}

void Utilities::print(list<Relation> r) {
    cout.width(1); cout << "";
    cout.width(vars.r_personname_length); cout << left << "Person's name:";
    cout.width(vars.r_compname_length); cout << left << "Computer name:";
    cout.width(vars.r_comptype_length); cout << right << "Computer type:" << endl;

    cout << " ";
    for(int j = 0; j < vars.r_line_length; j++) {
        cout << "-";
    }
    cout << endl;

    for(list<Relation>::iterator iter = r.begin(); iter != r.end(); iter++) {
        iter->print();
    }
}
