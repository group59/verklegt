#include "uiconsole.h"
#include "databaseinit.h"

// Initiates the connection to the SQL database and keeps it open
// while the program runs. The DatabaseInit is the used to start
// the UI Console
int main()
{
    system("Color B");
    DatabaseInit init = DatabaseInit();
    init.start();

    return 0;
}
