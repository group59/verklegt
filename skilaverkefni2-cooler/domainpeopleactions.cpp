#include "domainpeopleactions.h"

DomainPeopleActions::DomainPeopleActions() {
    prepo = PersonRepo();
}

//adds a new person and information about it to the database
//if the person is still alive, then yod is space
void DomainPeopleActions::add(string name, string gender, int yob, string s_yod) {
    int yod = 0;
    if(s_yod != "") {
        yod = atoi(s_yod.c_str());
    }

    if(validateName(name) && validateYears(yob, yod)) {
        Person p = Person(name, gender, yob, yod);
        prepo.add(p);
    }
    else {
        throw runtime_error("Adding Person failed. Some input was in the wrong format.");
    }
}

bool DomainPeopleActions::validateYears(int year1, int year2) {
    if(year2 == 0) {return validateYear(year1);}
    return validateYear(year1) && validateYear(year2) && year1 < year2;
}

// validates that an integer is at least 0 and below 2015
bool DomainPeopleActions::validateYear(int year) {
    return year < 2015 && year >= 0;
}

// validates that a string is at least 4 characters and has no numbers
bool DomainPeopleActions::validateName(string s) {
    for(string::iterator iter = s.begin(); iter != s.end(); iter++) {
        if(isdigit(*iter)) {
            cout << "Sorry, numbers are not allowed in a name!" << endl;
            return false;
        }
    }
    return s.length() > 3;
}

// 1 for Name, 2 for Gender, 3 for YOB, 4 for YOD
list<Person> DomainPeopleActions::getListByName(string toSortBy) {
    list<Person> p = prepo.viewList("1");
    if(toSortBy == "asc") {
        return p;
    }
    else {
        p.reverse();
        return p;
    }
}

list<Person> DomainPeopleActions::getListByGender() {
    list<Person> p = prepo.viewList("2");
    return p;
}

list<Person> DomainPeopleActions::getListByYOB(string toSortBy) {
    list<Person> p = prepo.viewList("3");
    if(toSortBy == "asc") {
        return p;
    }
    else {
        p.reverse();
        return p;
    }
}

list<Person> DomainPeopleActions::getListByYOD(string toSortBy) {
    list<Person> p = prepo.viewList("4");
    if(toSortBy == "asc") {
        return p;
    }
    else {
        p.reverse();
        return p;
    }
}

list<Person> DomainPeopleActions::search(string toSearch) {
    list<Person> p = prepo.search(toSearch);
    return p;
}

bool DomainPeopleActions::isRepoEmpty()
{
    return !prepo.tableExists();
}
