#ifndef DOMAINCOMPUTERACTIONS_H
#define DOMAINCOMPUTERACTIONS_H

#include <list>
#include <string>
#include <cstdlib>
#include <stdexcept>
#include "computer.h"
#include "computerrepo.h"
//#include "olddataaccess.h" // TO BE DELETED WHEN CONNECTION TO DB IS MADE
using namespace std;

class DomainComputerActions {

private:
//    OldDataAccess oda;
//    list<Computer> comp_list;
    ComputerRepo crepo;
    bool validateYear(int year);              // used to validate it integer is between 0 and 2015
    bool validateName(string s);   /* Used to validate that the string is at least 4 characters (according to
                                      http://www.answers.com/Q/What_is_the_shortest_name_in_the_world the shortest name
                                      possible is 4 characters. Also validates that there're no invalid characters in the string. */
    bool validateCompName(string s); // simply checks that the string is at least 2 characters
public:
    DomainComputerActions();           // constructor initiates an instance of Repo

    // used by UI layer, creates the Computer c and sends it onwards to the Data layer
    void add(string name, string s_yearBuilt, string type, bool wb);
    list<Computer> getListByName(string toSortBy);
    list<Computer> getListByYearBuilt(string toSortBy);
    list<Computer> getListByType(string toSortBy);
    list<Computer> getListByWasBuilt();
    list<Computer> search(string toSearch);
    bool isRepoEmpty();
};

#endif // DOMAINCOMPUTERACTIONS_H
