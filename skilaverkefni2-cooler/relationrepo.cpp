#include "relationrepo.h"

RelationRepo::RelationRepo()
{
    relationData = DataSqlAccess();
    tableName = "Relations";
    relationDB = relationData.getDatabaseConnection();
}

//if the table exists, returns true, else return false.
//if false is returned, the table (query) is created

bool RelationRepo::tableExists()
{
    if(relationDB.tables().contains(tableName))
    {
        return true;
    }
    else
    {
        return false;
    }
}

// creates a table in the database for this repo
void RelationRepo::createTable()
{
    QSqlQuery query(relationDB);

    if(!query.exec("CREATE TABLE "+tableName+"(SciID INTEGER, ComID INTEGER, "
                   "FOREIGN KEY (SciID) REFERENCES Scientists(ID), "
                   "FOREIGN KEY (ComID) REFERENCES Computers(ID), "
                   "PRIMARY KEY (SciID, ComID));"))
    {
        qDebug() << query.lastError();
    }
}

// take in two strings based on user input, UI sanitises input so we are
// sure that only one of each string exists when they are sent to this function
// we then find the ID based on the strings, change those to int
// those integer values are then pushed into the add function
void RelationRepo::findIDs(string person, string computer)
{
    QString personTemp = QString::fromStdString(person);
    QString computerTemp = QString::fromStdString(computer);

    QSqlQuery queryPerson(relationDB);
    QSqlQuery queryComputer(relationDB);

    int personID;
    int computerID;

    if(!queryPerson.exec("SELECT ID FROM Scientists s WHERE s.Name LIKE '%"+personTemp+"%'"))
    {
        qDebug() << queryPerson.lastError();
    }

    if(!queryComputer.exec("SELECT ID FROM Computers c WHERE c.Name LIKE '%"+computerTemp+"%'"))
    {
        qDebug() << queryComputer.lastError();
    }

    queryPerson.next();
    queryComputer.next();
    personID = queryPerson.value("ID").toInt();
    computerID = queryComputer.value("ID").toInt();

    add(personID, computerID);

}

// adds a database entry based on integers received
void RelationRepo::add(int personID, int computerID)
{
    if(!tableExists())
    {
        createTable();
    }

    QSqlQuery query(relationDB);

    query.prepare("INSERT INTO Relations(SciID, ComID) VALUES (:sciid, :comid)");
    query.bindValue(":sciid", personID);
    query.bindValue(":comid", computerID);

    if(!query.exec())
    {
        qDebug() << query.lastError();
    }
}

// prints out a raw list of every connection, not sorted
list<Relation> RelationRepo::viewList()
{
    list<Relation> relationList = list<Relation>();

    QSqlQuery query(relationDB);

    if(!query.exec("SELECT r.SciID AS 'personID', s.Name AS 'personName',"
                   " r.ComID AS 'computerID', c.Name AS 'computerName',"
                   " c.Type AS 'computerType' FROM Scientists s"
                   " INNER JOIN Relations r"
                   " ON r.SciID = s.ID"
                   " INNER JOIN Computers c"
                   " ON r.ComID = c.ID"))
    {
        qDebug() << query.lastError();
    }

    while(query.next())
    {
        Relation r = Relation();

        r.personID = query.value("personID").toInt();
        r.personName = query.value("personName").toString().toStdString();
        r.computerID = query.value("computerID").toInt();
        r.computerName = query.value("computerName").toString().toStdString();
        r.computerType = query.value("computerType").toString().toStdString();

        relationList.push_back(r);
    }

    return relationList;
}
