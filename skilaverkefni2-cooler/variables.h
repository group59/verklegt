#ifndef VARIABLES_H
#define VARIABLES_H

// A class to keep the length of columns for output/printing
class Variables {
    public:
    // Lengths used to print out lists of Person objects
    const static int p_name_length = 34, p_gender_length = 14, p_yearofbirth_length = 8, p_yearofdeath_length = 8;
    // Lengths used to print out lists of Computer objects
    const static int c_name_length = 26, c_yearbuilt_length = 14, c_type_length = 26, c_wasbuilt_length = 10;
    // Lengths used to print out lists of Relation objects
    const static int r_personname_length = 30, r_compname_length = 23, r_comptype_length = 23;
    // Lengths of the lines printed out between the column descriptions and the lists of objects
    const static int p_line_length = 64, c_line_length = 78, r_line_length = 78;
};

#endif // VARIABLES_H
