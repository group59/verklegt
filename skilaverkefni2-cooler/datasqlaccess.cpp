#include "datasqlaccess.h"

// handles all interactions and connections to the database (or database file)

DataSqlAccess::DataSqlAccess()
{
    connectionName = "verklegt";
}

// connects to a database if it exists, otherwise tries to create it
void DataSqlAccess::connect()
{
    database = QSqlDatabase::addDatabase("QSQLITE", connectionName);

    if(databaseExists())
    {
        database.setDatabaseName("verklegt-db.sqlite");
    }
    else
    {
        createDatabase();
    }

    database.open();
}

// disconnects link to a database (unless it has already been closed)
void DataSqlAccess::disconnect()
{
    if(database.isOpen())
    {
        cout << "Closing connection to database..." << endl;
        database.close();
    }
    else
    {
        cout << "Connection to database already closed." << endl;
    }

}

// checks if database exists
bool DataSqlAccess::databaseExists()
{
    QFileInfo info1("verklegt-db.sqlite");
    if(info1.exists())
    {
        return 1;
    }
    else
    {
        cout << "Database doesn't exist. ";
        return 0;
    }
}

// creates database if user selects y or Y, otherwise quits the program
void DataSqlAccess::createDatabase()
{
    string answer;
    cout << "To continue a database needs to be created. Do you want to create a database? (y/n) ";
    getline(cin, answer);
    cout << endl;
    if(answer == "y" || answer == "Y")
    {
        database.setDatabaseName("verklegt-db.sqlite");
    }
    else
    {
        exit(EXIT_SUCCESS);
    }
}

// checks if link to database is open and returns the connection
// safeguard if connection to database goes down for some reason
QSqlDatabase DataSqlAccess::getDatabaseConnection()
{
    QSqlDatabase db;

    if(QSqlDatabase::contains(connectionName))
    {
        db = QSqlDatabase::database(connectionName);
    }
    else
    {
        db = QSqlDatabase::addDatabase("QSQLITE", connectionName);
        db.setDatabaseName("verklegt-db.sqlite");

        db.open();
    }

    return db;
}
