#include "computerrepo.h"

ComputerRepo::ComputerRepo()
{
    computerData = DataSqlAccess();
    tableName = "Computers";
    computerDB = computerData.getDatabaseConnection();
}

// checks if the table for this repo exists
bool ComputerRepo::tableExists()
{
    if(computerDB.tables().contains(tableName))
    {
        return true;
    }
    else
    {
        return false;
    }
}

// creates a table, writes to qdebug if not successful
void ComputerRepo::createTable()
{
    QSqlQuery query(computerDB);

    if(!query.exec("CREATE TABLE "+tableName+"(ID INTEGER PRIMARY KEY AUTOINCREMENT, Name VARCHAR NOT NULL, YearBuilt INTEGER, Type VARCHAR NOT NULL, WasBuilt BOOL);"))
    {
        qDebug() << query.lastError();
    }
}

// adds a computer to the Computers table in the database
void ComputerRepo::add(Computer c)
{    
    if(!tableExists())
    {
        createTable();
    }

    QSqlQuery query(computerDB);

    query.prepare("INSERT INTO Computers (Name, YearBuilt, Type, WasBuilt)"
                  "VALUES(:name, :yearbuilt, :type, :wasbuilt);");
    query.bindValue(":name", QString::fromStdString(c.Name));
    query.bindValue(":yearbuilt", c.YearBuilt);
    query.bindValue(":type", QString::fromStdString(c.Type));
    query.bindValue(":wasbuilt", c.WasBuilt);

    if(!query.exec())
    {
        qDebug() << query.lastError();
    }
}

// lists up the table of computers based on user selection of the type of sort
// this function creates a string based on table name and the sortBy function
// and passes that string to the sortedList function
list<Computer> ComputerRepo::viewList(string toSort)
{
    list<Computer> computers = list<Computer>();

    QString queryString;

    queryString = "SELECT * FROM "+ tableName +" "+ sortBy(toSort);

    computers = sortedList(queryString);

    return computers;

}

// returns a string based on what type of sort the user selected
QString ComputerRepo::sortBy(string toSort)
{
    QString tempString;

    if(toSort == "1")
    {
        tempString = "ORDER BY Name";
    }
    else if(toSort == "2")
    {
        tempString = "ORDER BY YearBuilt";
    }
    else if(toSort == "3")
    {
        tempString = "ORDER BY Type";
    }
    else if(toSort == "4")
    {
        tempString = "ORDER BY WasBuilt";
    }
    else
    {
        tempString = "ORDER BY ID";
    }

    return tempString;
}

// creates a sorted list based on string recieved
list<Computer> ComputerRepo::sortedList(QString queryString)
{
    list<Computer> sortedComputers = list<Computer>();

    QSqlQuery query(computerDB);

    if(!query.exec(queryString))
    {
        qDebug() << query.lastError();
    }

    while(query.next())
    {
        Computer c = Computer();
        c.Name = query.value("Name").toString().toStdString();
        c.YearBuilt = query.value("YearBuilt").toInt();
        c.Type = query.value("Type").toString().toStdString();
        c.WasBuilt = query.value("WasBuilt").toBool();

        sortedComputers.push_back(c);
    }

    return sortedComputers;
}

// creates a querystring based on string received to be used in sortedList function
// searches for name of computer
list<Computer> ComputerRepo::search(string toFind)
{
    QString tempFind = QString::fromStdString(toFind);

    list<Computer> searchComputer;

    QString queryString = "SELECT * FROM "+tableName+" c WHERE c.Name LIKE '%"+tempFind+"%'";

    searchComputer = sortedList(queryString);

    return searchComputer;
}
