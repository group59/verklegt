#include "domainrelationactions.h"

DomainRelationActions::DomainRelationActions() {
    rrepo = RelationRepo();
}

void DomainRelationActions::add(string first, string second) {
    rrepo.findIDs(first, second);
}

list<Relation> DomainRelationActions::getAllRelations() {
    list<Relation> r = rrepo.viewList();
    return r;
}
