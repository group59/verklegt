#ifndef COMPUTERREPO_H
#define COMPUTERREPO_H

#include "datasqlaccess.h"
#include "computer.h"

// handles all database interactions regarding computers
class ComputerRepo
{
public:
    ComputerRepo();
    void add(Computer c);
    list<Computer> viewList(string toSort);
    list<Computer> search(string toFind);
    bool tableExists();
private:
    DataSqlAccess computerData;
    QString tableName;
    QSqlDatabase computerDB;
    void createTable();
    list<Computer> sortedList(QString queryString);
    QString sortBy(string toSort);
};

#endif // COMPUTERREPO_H
