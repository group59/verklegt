#include "uiconsole.h"

UIConsole::UIConsole() {
    util = Utilities();
    people_actions = DomainPeopleActions();
    comp_actions = DomainComputerActions();
    rel_actions  = DomainRelationActions();
}

void UIConsole::start() {
    welcome();
    cout << endl;
    help();
    repl();
}

void UIConsole::help() {
    cout << endl << " The following commands are available:" << endl << " ";
    for(int i = 0; i < 37; i++) {
        cout << "-";
    }
    cout << endl;
    cout << " help:            Prints this help menu." << endl
         << " add:             Adds a person or a computer to the database." << endl
         << " connect:         Makes a connection between a person and a computer" << endl
         << "                  in the database." << endl
         << " list:            Gives the options to view a list of all the people" << endl
         << "                  in the database or all the computers in the database." << endl
         << "                  Either list can be ordered in certain ways." << endl
         << " search:          Search for people or computers." << endl
         << " listcon:         Lists all connections in the database." << endl
         << " exit/quit:       Exits the Database Manager." << endl;
}

void UIConsole::repl() {
    string cmd;
    do {
        cout << endl << "Database Manager> ";
    } while(getline(cin, cmd) && execute_command(cmd));   // magic :) keeps the UI going while the
                                                   // input isn't either quit or exit
    cout << endl << "Exiting. Goodbye :(" << endl << endl;
}

void UIConsole::welcome() {
    ifstream menu ("user_menustart.txt");
    cout << menu.rdbuf();
}

void UIConsole::clear_screen() {
    for(int i = 0; i < 40; i++) {
        cout << "\n";
    }
}

bool UIConsole::execute_command(string cmd) {
    string command = "";
    for(string::iterator iter = cmd.begin(); iter != cmd.end(); iter++) {
        command += tolower(*iter);
    }

    if(command == "help") {
        help();
    }
    else if(command == "list") {
        while(viewList());
    }
    else if(command == "add") {
        while(add());
    }
    else if(command == "connect") {
        while(getConnectionInfo());
    }
    else if(command == "search") {
        while(search());
    }
    else if(command == "listcon") {
        listConnections();
    }
    else if(command == "") { }
    else if(command == "welcome") {
        clear_screen();
        welcome();
        help();
    }
    else if(command == "quit" || command == "exit") {
        return false;
    }
    else {
        cout << endl << "Unknown command '" << cmd << "'" << endl;
    }
    return true;
}

// Outputs a list of commands, asks for input and prints out the lists accordingly
bool UIConsole::viewList() {
    if(people_actions.isRepoEmpty() && comp_actions.isRepoEmpty()) {
        cout << endl << "The database seems to be empty. So sorry" << endl
             << "but there's nothing to print out." << endl
             << " Returning to the Database Manager." << endl << endl;
        return false;
    }
    else if(people_actions.isRepoEmpty()) {
        cout << endl << "There are no people to print out." << endl;
        while(viewListComputers());
    }
    else if(comp_actions.isRepoEmpty()) {
        cout << endl << "There are no computers to print out." << endl;
        while(viewListPeople());
    }
    else {
        cout << endl << "Please enter: " << endl << "1: To view all the people" << endl
           << "2: To view all the computers" << endl
           << "Note that if any other command than the ones listed is entered," << endl
           << "both lists will be printed, sorted by name in ascending order." << endl
           << "> ";

        string cmd = "";
        getline(cin, cmd);
        cout << endl;

        if(cmd == "1") {
            while(viewListPeople());
        }
        else if(cmd == "2") {
            while(viewListComputers());
        }
        else {
            if(!people_actions.isRepoEmpty() && !comp_actions.isRepoEmpty()) {
                util.print(people_actions.getListByName("asc"),
                           comp_actions.getListByName("asc"));
            }
            else if(!people_actions.isRepoEmpty()){
                util.print(people_actions.getListByName("asc"));
            }
            else {
                util.print(comp_actions.getListByName("asc"));
            }
        }
    }
    return false;
}

//options for how the lists can be sorted
//asks for the integer input from 1 to 7
//both for sorting list of people and
//list of computers
bool UIConsole::viewListPeople() {
    cout << endl
         << "Here are the options for printing out the lists of all the" << endl
         << "people in the database. Pick a number between 1 and 7." << endl  << endl
         << "1:  List people by name in ascending order." << endl
         << "2.  List people by name in descending order." << endl
         << "3:  List people by gender." << endl
         << "4:  List people by year of birth in ascending order." << endl
         << "5:  List people by year of birth in descending order." << endl
         << "6:  List people by year of death in ascending order." << endl
         << "7:  List people by year of death in descending order." << endl
         << "> ";

    string cmd = "";
    getline(cin, cmd);
    cout << endl;
    list<Person> p;

    if(cmd == "1") {
        p = people_actions.getListByName("asc");
    }
    else if(cmd == "2") {
        p = people_actions.getListByName("");
    }
    else if(cmd == "3") {
        p = people_actions.getListByGender();
    }
    else if(cmd == "4") {
        p = people_actions.getListByYOB("asc");
    }
    else if(cmd == "5") {
        p = people_actions.getListByYOB("");
    }
    else if(cmd == "6") {
        p = people_actions.getListByYOD("asc");
    }
    else if(cmd == "7") {
        p = people_actions.getListByYOD("");
    }
    else {
        cout << endl << "Invalid option. Returning to the Database Manager." << endl;
        return false;
    }
    util.print(p);
    return false;
}

bool UIConsole::viewListComputers() {
    cout << endl
         << "Here are the options for printing out the lists of all the" << endl
         << "computers in the database. Pick a number between 1 and 7." << endl  << endl
         << "1: List computers by name in ascending order." << endl
         << "2: List computers by name in descending order." << endl
         << "3: List computers by type in ascending order." << endl
         << "4: List computers by type in descending order." << endl
         << "5: List computers by if they were built." << endl
         << "6: List computers by the year they were built in ascending order." << endl
         << "7: List computers by the year they were built in descending order." << endl
         << "> ";

    string cmd = "";
    getline(cin, cmd);
    cout << endl;
    list<Computer> c;

    if(cmd == "1") {
        c = comp_actions.getListByName("asc");
    }
    else if(cmd == "2") {
        c = comp_actions.getListByName("");
    }
    else if(cmd == "3") {
        c = comp_actions.getListByType("asc");
    }
    else if(cmd == "4") {
        c = comp_actions.getListByType("");
    }
    else if(cmd == "5") {
        c = comp_actions.getListByWasBuilt();
    }
    else if(cmd == "6") {
        c = comp_actions.getListByYearBuilt("asc");
    }
    else if(cmd == "7") {
        c = comp_actions.getListByYearBuilt("");
    }
    else {
        cout << endl << "Invalid option. Returning to the Database Manager." << endl;
        return false;
    }
    util.print(c);
    return false;
}

bool UIConsole::add() {
    cout << endl << "Do you want to add a person or a computer?" << endl
         << "1. Person" << endl << "2. Computer" << endl << "> ";
    string whatToAdd = "", cmd;
    getline(cin, whatToAdd);

    if(whatToAdd == "1" || whatToAdd == "person") {
        while(getPersonInfo());
    }
    else if(whatToAdd == "2" || whatToAdd == "computer") {
        while(getComputerInfo());
    }
    else {
        cout << "You didn't pick a valid option. Do you want to" << endl
             << "try again?" << endl << "> ";
        string check = "";
        getline(cin, check);
        if(check == "yes" || check == "Yes" || check == "y" || check == "Y") {
            cout << endl << "Adding aborted. Returning to the Database Manager." << endl;
            return true;
        }
    }
    return false;
}

bool UIConsole::getPersonInfo() {
    string name = "", gender = "", yod = "";
    int yob = 0;

    cout << "Enter the person's name: ";
    getline(cin, name);

    cout << "Enter either 1 or 2 to pick the person's gender:" << endl
         << "1. Male" << endl << "2. Female" << endl << "> ";

    string checkGender;
    getline(cin, checkGender);
    cout << endl;

    if(checkGender == "2") {gender = "Female";}
    else if(checkGender == "1") {gender = "Male";}
    else {
        cout << "You didn't pick a valid option." << endl
             << "Returning to the Database Manager." << endl;
        return false;
    }

    cout << "Enter the person's year of birth: ";
    cin >> yob;
    cin.ignore();
    cout << "Enter the person's year of death (press enter if person's still alive): ";
    getline(cin, yod); //allows to send in empty string or a string with spaces

    try {
        people_actions.add(name, gender, yob, yod);
    } catch(...) {
        cout << "Adding the person to the database failed. Probably because" << endl
             << "some input was in the wrong format. Please try again." << endl;
        return false;
    }
    cout << endl << "Person has been added to the database." << endl
         << "Returning to the Database Manager." << endl;
    return false;
}

bool UIConsole::getComputerInfo() {
    string name = "", s_yearBuilt = "0", type = "";
    bool wasBuilt = true;

    cout << "Enter the computer's name: ";
    getline(cin, name);

    cout << "Enter a number between 1 and 7 to pick the computer's type:" << endl
         << "1. Mechanical calculator" << endl << "2. Mechanical computer" << endl
         << "3. Electro-mechanical counter" << endl << "4. Vacuum tube computer" << endl
         << "5. Transistorized computer" << endl << "6. Integrated circuit computer" << endl
         << "7. Digital computer" << endl
         << "You can also pick option 8 if you want to enter the computer's" << endl
         << "type yourself." << endl
         << "> ";

    string checkType = "";
    getline(cin, checkType);

    if(checkType == "1") {type = "Mechanical calculator";}
    else if(checkType == "2") {type = "Mechanical computer";}
    else if(checkType == "3") {type = "Electro-mechanical counter";}
    else if(checkType == "4") {type = "Vacuum tube computer";}
    else if(checkType == "5") {type = "Transistorized computer";}
    else if(checkType == "6") {type = "Integrated circuit computer";}
    else if(checkType == "7") {type = "Digital computer";}
    else if(checkType == "8") {
        cout << "Enter the computer's type: ";
        getline(cin, type);
    }
    else {
        cout << endl << "Invalid option. Returning to the Database Manager." << endl;
        return false;
    }

    cout << "Was the computer built?" << endl
         << "Pick either 1 for 'True' or 2 for 'False': " << endl
         << "> ";
    string trueOrFalse = "";
    getline(cin, trueOrFalse);

    if(trueOrFalse == "1") {}
    else if(trueOrFalse == "2") {
        wasBuilt = false;
    }
    else {
        cout << endl << "Invalid option. Returning to the Database Manager." << endl;
        return false;
    }

    if(wasBuilt) {
        cout << "Enter the year that this computer was built (press enter if it wasn't built): ";
        getline(cin, s_yearBuilt);
    }

    try {
        comp_actions.add(name, s_yearBuilt, type, wasBuilt);
    } catch(...) {
        cout << "Adding the computer to the database failed. Probably because" << endl
             << "some input was in the wrong format. Please try again." << endl;
        return false;
    }
    cout << endl << "Computer has been added to the database." << endl
         << "Returning to the Database Manager." << endl;
    return false;
}

bool UIConsole::getConnectionInfo() {
    cout << endl << "To add a connection between a person and a computer," << endl
         << "enter the name of the person. If there're more than" << endl
         << "one person with that name you have to be more specific." << endl
         << "Enter the name to search for here:" << endl;

    string personToAdd = "";
    getline(cin, personToAdd);
    list<Person> p = people_actions.search(personToAdd);
    if(p.size() == 0) {
        cout << "There're no people with '" << personToAdd << "' in their" << endl
             << "name. Do you want to search again? ";
        string cmd = "";
        getline(cin, cmd);
        if(cmd == "yes" || cmd == "Yes" || cmd == "y" || cmd == "Y") {
            getConnectionInfo();
            return false;
        }
        return false;
    }
    else if(p.size() != 1) {
        cout << "There're too many people with '" << personToAdd << "' in their" << endl
             << "name. Please refine your search. Do you want to search again? " << endl;
        string cmd = "";
        getline(cin, cmd);
        if(cmd == "yes" || cmd == "Yes" || cmd == "y" || cmd == "Y") {
            getConnectionInfo();
            return false;
        }
        return false;
    }
    else {
        Person pers = p.front();
        string p_name = pers.Name;
        while(secondGetConnectionInfo(p_name));
    }
    return false;
}

bool UIConsole::secondGetConnectionInfo(string p_name) {
    cout << endl << "Now we have to search for the computer that we're" << endl
         << "going to connect with " << p_name << endl << endl
         << "If there're more than one computer with that name" << endl
         << "you have to be more specific." << endl
         << "Enter the name to search for here: " << endl;

    string computerToAdd = "";
    getline(cin, computerToAdd);
    list<Computer> c = comp_actions.search(computerToAdd);
    if(c.size() == 0) {
        cout << "There're no computers with '" << computerToAdd << "' in their" << endl
             << "name. Do you want to search again? ";
        string cmd = "";
        getline(cin, cmd);
        if(cmd == "yes" || cmd == "Yes" || cmd == "y" || cmd == "Y") {
            secondGetConnectionInfo(p_name);
            return false;
        }
        else {
            return false;
        }
    }
    else if(c.size() != 1) {
        cout << "There're too many computers with '" << computerToAdd << "' in their" << endl
             << "name. Please refine your search. Do you want to search again? ";
        string cmd = "";
        getline(cin, cmd);
        if(cmd == "yes" || cmd == "Yes" || cmd == "y" || cmd == "Y") {
            secondGetConnectionInfo(p_name);
            return false;
        }
        else {
            return false;
        }
    }
    else {
        Computer comp = c.front();
        string c_name = comp.Name;
        try {
            rel_actions.add(p_name, c_name);
            cout << endl << "A connection between '" << p_name << "' and '" << endl
                 << c_name << "' has been made." << endl << endl;
        } catch(...) {
            cout << endl << "There was an error with adding the connection." << endl
                 << "Returning to the Database Manager." << endl << endl;
        }
    }
    return false;
}

bool UIConsole::search() {
    if(people_actions.isRepoEmpty() && comp_actions.isRepoEmpty()) {
        cout << endl << "The database seems to be empty. So sorry" << endl
             << "but there's no data to search in." << endl
             << "Returning to the Database Manager." << endl << endl;
        return false;
    }
    else if(people_actions.isRepoEmpty()) {
        cout << endl << "There are no people to search for." << endl
             << "Do you want to search for a computer? ";
        string cmd = "";
        getline(cin, cmd);
        if(cmd == "yes" || cmd == "Yes" || cmd == "y" || cmd == "Y") {
            searchForComputer();
            cout << endl;
            return false;
        }
        else {
            cout << "Invalid option. Returning to the Database Manager." << endl;
            return false;
        }
    }
    else if(comp_actions.isRepoEmpty()) {
        cout << endl << "There are no computers to search for." << endl
             << "Do you want to search for a person? ";
        string cmd = "";
        getline(cin, cmd);
        if(cmd == "yes" || cmd == "Yes" || cmd == "y" || cmd == "Y") {
            searchForPeople();
            cout << endl;
            return false;
        }
        else {
            cout << "Invalid option. Returning to the Database Manager." << endl;
            return false;
        }
    }
    else {
        cout << "Pick either 1 or 2 if you want to search for a" << endl
             << "1. Person " << endl << "2. Computer " << endl << "> ";
        string cmd = "";
        getline(cin, cmd);
        if(cmd == "1") {
            searchForPeople();
            cout << endl;
            return false;
        }
        else if(cmd == "2") {
            searchForComputer();
            cout << endl;
            return false;
        }
        else {
            cout << "Invalid option. Returning to the Database Manager." << endl;
            return false;
        }
    }
    return false;
}

void UIConsole::searchForPeople() {
    cout << "Enter what you want to search for: ";
    string toSearchFor = "";
    getline(cin, toSearchFor);
    list<Person> p = people_actions.search(toSearchFor);
    if(p.empty()) {
        cout << endl << "There were no people in the database with" << endl
             << "'" << toSearchFor << "' in their name." << endl
             << "Returning to the Database Manager." << endl;
    }
    else {
        cout << endl << "This is what was found: " << endl << endl;
        util.print(p);
    }
}

void UIConsole::searchForComputer() {
    cout << "Enter what you want to search for: ";
    string toSearchFor = "";
    getline(cin, toSearchFor);
    list<Computer> c = comp_actions.search(toSearchFor);
    if(c.empty()) {
        cout << endl << "There were no computers in the database with" << endl
             << "'" << toSearchFor << "' in their name." << endl
             << "Returning to the Database Manager." << endl;
    }
    else {
        cout << endl << "This is what was found:" << endl << endl;
        util.print(c);
    }
}

void UIConsole::listConnections() {
    list<Relation> r = rel_actions.getAllRelations();
    if(r.empty()) {
        cout << endl << "There're no connections in the database." << endl
             << endl << "Returning to the Database Manager." << endl;
    }
    else {
        cout << endl << "These are all the connections that have been made:" << endl << endl;
        util.print(r);
    }
}
