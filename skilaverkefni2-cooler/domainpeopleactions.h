#ifndef DOMAINPEOPLEACTIONS_H
#define DOMAINPEOPLEACTIONS_H

#include <list>
#include <string>
#include <cstdlib>
#include <stdexcept>
#include "person.h"
#include "personrepo.h"
//#include "olddataaccess.h" // TO BE DELETED WHEN CONNECTION TO DB IS MADE
using namespace std;

class DomainPeopleActions {

private:
    PersonRepo prepo;
    bool validateYear(int year);       // used to validate it integer is between 0 and 2015
    bool validateYears(int year1, int year2);
    bool validateName(string s);   /* Used to validate that the string is at least 4 characters (according to
                                      http://www.answers.com/Q/What_is_the_shortest_name_in_the_world the shortest name
                                      possible is 4 characters. Also validates that there're no invalid characters in the string. */
public:
    DomainPeopleActions();           // constructor initiates an instance of PeopleRepo

    // used by UI layer, validates input, creates the Person p and sends it onwards to the Data layer
    void add(string name, string gender, int yob, string s_yod);

    // searches for a name by prefix and outputs to the screen. Asks how to sort the list if there're more than 1 entries found
    list<Person> getListByName(string toSortBy);
    list<Person> getListByGender();
    list<Person> getListByYOB(string toSortBy);
    list<Person> getListByYOD(string toSortBy);
    list<Person> search(string toSearch);
    bool isRepoEmpty();
};

#endif // DOMAINPEOPLEACTIONS_H
