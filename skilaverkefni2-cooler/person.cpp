#include "person.h"

Person::Person() {
    Name = "";
    Gender = "";
    YearOfBirth = 0;
    YearOfDeath = 0;
}

Person::Person(string name, string gender, int yob, int yod) {
    Name = name;
    Gender = gender;
    YearOfBirth = yob;
    YearOfDeath = yod;
}

//prints people from the database
//if the person is still alive
//yod is just a space
void Person::print() {
    cout.width(1); cout << "";
    cout.width(vars.p_name_length); cout << left << Name;
    cout.width(vars.p_gender_length); cout << left << Gender;
    cout.width(vars.p_yearofbirth_length); cout << left << YearOfBirth;
    if(YearOfDeath == 0) {
        cout.width(vars.p_yearofdeath_length); cout << right << " " << endl;
    } else {
        cout.width(vars.p_yearofdeath_length); cout << right << YearOfDeath << endl;
    }
}
