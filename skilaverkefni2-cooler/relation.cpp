#include "relation.h"

Relation::Relation()
{
    personID = 0;
    personName = "";
    computerID = 0;
    computerName = "";
    computerType = "";
}

void Relation::print() {
    cout.width(1); cout << "";
    cout.width(vars.r_personname_length); cout << left << personName;
    cout.width(vars.r_compname_length); cout << left << computerName;
    cout.width(vars.r_comptype_length); cout << right << computerType << endl;
}
