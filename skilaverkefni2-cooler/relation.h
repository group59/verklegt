#ifndef RELATION_H
#define RELATION_H

#include "variables.h"
#include <string>
#include <iostream>
using namespace std;

class Relation {
    private:
        Variables vars;
    public:
        Relation();
        int personID;
        string personName;
        int computerID;
        string computerName;
        string computerType;
        void print();
};

#endif // RELATION_H
