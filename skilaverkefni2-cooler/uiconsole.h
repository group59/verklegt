#ifndef UICONSOLE_H
#define UICONSOLE_H
#include "utilities.h"
#include "person.h"
#include "computer.h"
#include "relation.h"
#include "domainpeopleactions.h"
#include "domaincomputeractions.h"
#include "domainrelationactions.h"
#include <list>
#include <string>
#include <fstream>

class UIConsole {

private:
    DomainPeopleActions people_actions;
    DomainComputerActions comp_actions;
    DomainRelationActions  rel_actions;
    Utilities util;
    bool execute_command(string cmd);
    void help();               // prints out the help menu
    void repl();
    void welcome();            // displays the awesome welcome message
    void clear_screen();       // clears the screen by printing 40 \n characters
    bool viewList();           // called upon with the list command. Askes how to sort the
                               // list and then prints it to the screen
    bool viewListPeople();
    bool viewListComputers();
    bool add();                // checks what the user wants to add and calls appropriate function
    bool getPersonInfo();      // gets info about the Person to be added and sends it onwards
    bool getComputerInfo();    // gets info about the Computer to be added and sends it onwards
    bool getConnectionInfo();  // gets info about the Connection to be added and sends it onwards
    bool secondGetConnectionInfo(string p_name);
    bool search();
    void searchForPeople();
    void searchForComputer();
    void listConnections();  // gets a list of Relations objects and prints them out :)
public:
    UIConsole();
    void start();              // starts the UI, calls the welcome, help and repl functions
};

#endif // UICONSOLE_H
