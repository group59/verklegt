#include "exitdialog.h"
#include "ui_exitdialog.h"

ExitDialog::ExitDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ExitDialog)
{
    ui->setupUi(this);
}

ExitDialog::~ExitDialog()
{
    delete ui;
}

bool ExitDialog::on_buttonBox_ToExit_accepted() {
    return true;
}

bool ExitDialog::on_buttonBox_ToExit_rejected() {
    return false;
}
