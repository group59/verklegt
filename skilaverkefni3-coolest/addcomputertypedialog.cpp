#include "addcomputertypedialog.h"
#include "ui_addcomputertypedialog.h"

addComputerTypeDialog::addComputerTypeDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::addComputerTypeDialog)
{
    ui->setupUi(this);
}

addComputerTypeDialog::~addComputerTypeDialog()
{
    delete ui;
}

// closing the window
void addComputerTypeDialog::on_addComputerTypeCancelButton_clicked()
{
    this->close();
}

// validating the user input
bool addComputerTypeDialog::nameValidation()
{
    comType = comAct.getAllTypes();

    ui->addComputerTypeErrorLabel->setText("");

    if(ui->addComputerTypeInput->text().isEmpty())
    {
        ui->addComputerTypeErrorLabel->setText("<span style='color:red'>Type needs to have a name</span>");
        return false;
    }
    if(ui->addComputerTypeInput->text().length() < 2)
    {
        ui->addComputerTypeErrorLabel->setText("<span style='color:red'>Type has a minimum of two characters</span>");
        return false;
    }
    for(list<string>::iterator iter = comType.begin(); iter != comType.end(); iter++)
    {
        string input = ui->addComputerTypeInput->text().toStdString();
        if(input == iter->c_str())
        {
            ui->addComputerTypeErrorLabel->setText("<span style='color:red'>Type already exists!</span>");
            return false;
        }
    }

    return true;
}

// adding the input to a table through DomainComputerActions class
// and closes the window after that
void addComputerTypeDialog::on_addComputerTypeAddButton_clicked()
{
    if(nameValidation())
    {
        string name = ui->addComputerTypeInput->text().toStdString();

        comAct.addType(name);

        this->close();
    }
}
