#include "utilities.h"

// borrowed from code from a lecture
string Utilities::stringToLower(string original) {
    string result = "";

    for(unsigned int i = 0; i < original.length(); ++i)
    {
        result += std::tolower(original[i]);
    }

    return result;
}

void Utilities::searchGoogle(string stringToSearch) {
    system(searchstring(stringToSearch)); // borrowed the method from the lottery code so I think
}                                         // it's ok to use the system command for this...
                                          // Please do correct me if I'm wrong: kristinnf13@ru.is


// adds a string that is going to be googled to the prefix and changes
// it to a character pointer that the system command can use
char *Utilities::searchstring(string toChange) {
    string startOfString = "start chrome https://www.google.is/webhp#q=";

    replace(toChange.begin(), toChange.end(), ' ', '+'); // replaces all spaces with '+'
    string finalString = startOfString + toChange;

    char *c = new char[finalString.size() + 1];          // and here the final string is changed
    copy(finalString.begin(), finalString.end(), c);     // into a character pointer
    c[finalString.size()] = '\0';

    return c;
}
