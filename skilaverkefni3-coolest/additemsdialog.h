#ifndef ADDITEMSDIALOG_H
#define ADDITEMSDIALOG_H

#include <QDialog>
#include <QComboBox>
#include <string>
#include "Domain-Actions/domaincomputeractions.h"
#include "Domain-Actions/domainscientistsactions.h"
#include "Domain-Actions/domainrelationsactions.h"
#include "addcomputertypedialog.h"

// this handles gui interactions for adding a scientist, a computer or a connection

namespace Ui {
class addItemsDialog;
}

class addItemsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit addItemsDialog(QWidget *parent = 0);
    ~addItemsDialog();

private slots:

    void on_computerAddButton_clicked();
    void on_computerClearButton_clicked();
    void on_computerCancelButton_clicked();
    void on_computerNameInput_editingFinished();
    void on_computerWasbuiltCheck_toggled(bool checked);
    void on_scientistDeadCheck_toggled(bool checked);
    void on_scientistCancelButton_clicked();
    void on_scientistClearButton_clicked();
    void on_inventorCancelButton_clicked();
    void on_inventorClearButton_clicked();
    void on_inventorAddButton_clicked();
    void on_scientistAddButton_clicked();
    void on_scientistNameInput_editingFinished();
    void on_addItemsTabs_tabBarClicked(int index);
    void on_computerTypeAddButton_clicked();

private:
    Ui::addItemsDialog *ui;

    DomainComputerActions computerAct;
    DomainScientistsActions scientistAct;
    DomainRelationsActions relationAct;

    list<string> currentTypes;
    list<QString> yearSelection;
    list<Scientist> currentScientists;
    list<Computer> currentComputers;

    list<QString> getYearSelection();
    bool computerNameInputValid();
    bool computerAddValidate();
    void scientistErrorLabelsClear();
    bool scientistNameInputValid();
    bool scientistAddValidate();
    void computerErrorLabelsClear();
    void inventorErrorLabelsClear();
    bool inventorAddValidate();
    void getComputerTypes();
    void getScientistYearBorn();
    void getScientistYearDied();
    void getComputerYearBuilt();
    void getInvScientists();
    void getInvComputers();
    void setScientistDefault();
    void setComputerDefault();
    void setInventorDefault();
};

#endif // ADDITEMSDIALOG_H
