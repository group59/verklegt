#ifndef LISTANDSEARCHDIALOG_H
#define LISTANDSEARCHDIALOG_H

#include <QDialog>
#include "Containers/scientist.h"
#include "Containers/computer.h"
#include "utilities.h"
#include "Domain-Actions/domainscientistsactions.h"
#include "Domain-Actions/domaincomputeractions.h"

namespace Ui {
class ListAndSearchDialog;
}

class ListAndSearchDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ListAndSearchDialog(QWidget *parent = 0);
    ~ListAndSearchDialog();

private slots:
    void on_lineEdit_inputSciForSearch_textChanged(const QString &arg1);
    void on_lineEdit_inputCompForSearch_textChanged(const QString &arg1);
    void on_table_listOfScientists_doubleClicked(const QModelIndex &index);
    void on_table_listOfComputers_doubleClicked(const QModelIndex &index);

private:
    Utilities util;
    void getAllScientists();
    void displayAllScientists();
    void getAllComputers();
    void displayAllComputers();

    Ui::ListAndSearchDialog *ui;

    DomainScientistsActions sci_actions;
    DomainComputerActions comp_actions;

    list<Scientist> currentScientists;
    list<Scientist> currentDisplayedScientists;
    list<Computer> currentComputers;
    list<Computer> currentDisplayedComputers;
};

#endif // LISTANDSEARCHDIALOG_H
