#-------------------------------------------------
#
# Project created by QtCreator 2014-12-14T23:34:19
#
#-------------------------------------------------

QT       += core sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
    datasqlaccess.cpp \
    Containers/computer.cpp \
    Containers/relation.cpp \
    Containers/scientist.cpp \
    Repositories/computerrepo.cpp \
    Repositories/relationrepo.cpp \
    Repositories/scientistrepo.cpp \
    Domain-Actions/domainscientistsactions.cpp \
    Domain-Actions/domaincomputeractions.cpp \
    Domain-Actions/domainrelationsactions.cpp \
    listandsearchdialog.cpp \
    databaseinit.cpp \
    exitdialog.cpp \
    utilities.cpp \
    createdatabasedialog.cpp \
    listbyconnectiondialog.cpp \
    additemsdialog.cpp \
    aboutdialog.cpp \
    addcomputertypedialog.cpp

HEADERS  += mainwindow.h \
    datasqlaccess.h \
    Containers/computer.h \
    Containers/relation.h \
    Containers/scientist.h \
    Repositories/computerrepo.h \
    Repositories/relationrepo.h \
    Repositories/scientistrepo.h \
    Domain-Actions/domainscientistsactions.h \
    Domain-Actions/domaincomputeractions.h \
    Domain-Actions/domainrelationsactions.h \
    listandsearchdialog.h \
    databaseinit.h \
    exitdialog.h \
    utilities.h \
    createdatabasedialog.h \
    listbyconnectiondialog.h \
    additemsdialog.h \
    aboutdialog.h \
    addcomputertypedialog.h

FORMS    += mainwindow.ui \
    listandsearchdialog.ui \
    exitdialog.ui \
    createdatabasedialog.ui \
    listbyconnectiondialog.ui \
    additemsdialog.ui \
    aboutdialog.ui \
    addcomputertypedialog.ui

RESOURCES += \
    Resources/Resources.qrc
