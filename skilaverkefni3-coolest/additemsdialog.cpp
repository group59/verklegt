#include "additemsdialog.h"
#include "ui_additemsdialog.h"

addItemsDialog::addItemsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::addItemsDialog)
{
    ui->setupUi(this);

    scientistAct = DomainScientistsActions();
    computerAct = DomainComputerActions();
    relationAct = DomainRelationsActions();

    getScientistYearBorn();
    getScientistYearDied();
    getComputerTypes();
    getComputerYearBuilt();
    getInvScientists();
    getInvComputers();
}

addItemsDialog::~addItemsDialog()
{
    delete ui;
}

//--- validation starts ---//
// validates input from user in names fields //
bool addItemsDialog::computerNameInputValid()
{
    ui->computerNameErrorLabel->setText("");

    if(ui->computerNameInput->text().isEmpty())
    {
        ui->computerNameErrorLabel->setText("<span style='color:red'>You need to input a name</span>");
        return false;
    }
    if(ui->computerNameInput->text().length() < 3)
    {
        ui->computerNameErrorLabel->setText("<span style='color:red'>Name of computer needs to be longer than 2 letters</span>");
        return false;
    }

    return true;
}

bool addItemsDialog::scientistNameInputValid()
{
    ui->scientistNameErrorLabel->setText("");

    if(ui->scientistNameInput->text().isEmpty())
    {
        ui->scientistNameErrorLabel->setText("<span style='color:red'>You need to input a name</span>");
        return false;
    }
    if(ui->scientistNameInput->text().length() < 4)
    {
        ui->scientistNameErrorLabel->setText("<span style='color:red'>Name needs to be longer than 4 letters</span>");
        return false;
    }

    string sciName = ui->scientistNameInput->text().toStdString();
    for(string::iterator iter = sciName.begin(); iter != sciName.end(); iter++)
    {
        if(isdigit(*iter))
        {
            ui->scientistNameErrorLabel->setText("<span style='color:red'>There are no numbers allowed in a name</span>");
            return false;
        }
    }

    return true;
}

// checks when Add button is pushed whether all relevant fields are in default state or not //
// if in default state a errormessage is shown //
bool addItemsDialog::computerAddValidate()
{
    computerErrorLabelsClear();

    if(!computerNameInputValid())
    {
        return false;
    }
    if(ui->computerTypeDropdown->currentIndex() == 0)
    {
        ui->computerTypeErrorLabel->setText("<span style='color:red'>Select a valid type</span>");
        return false;
    }
    if(ui->computerWasbuiltCheck->isChecked())
    {
        if(ui->computerYearbuiltDropdown->currentIndex() == 0)
        {
            ui->computerYearbuiltErrorLabel->setText("<span style='color:red'>Select a valid year</span>");
            return false;
        }
    }

    return true;
}

bool addItemsDialog::scientistAddValidate()
{
    scientistErrorLabelsClear();

    if(!scientistNameInputValid())
    {
        return false;
    }
    if(ui->scientistGenderDropdown->currentIndex() == 0)
    {
        ui->scientistGenderErrorLabel->setText("<span style='color:red'>Select a valid gender</span>");
        return false;
    }
    if(ui->scientistYearofbirthDropdown->currentIndex() == 0)
    {
        ui->scientistYearofbirthErrorLabel->setText("<span style='color:red'>Select a valid year of birth</span>");
        return false;
    }
    if(ui->scientistDeadCheck->isChecked())
    {
        if(ui->scientistYearofdeathDropdown->currentIndex() == 0)
        {
            ui->scientistYearofdeathErrorLabel->setText("<span style='color:red'>Select a valid year of death</span>");
            return false;
        }
        if(ui->scientistYearofdeathDropdown->currentText() < ui->scientistYearofbirthDropdown->currentText())
        {
            ui->scientistYearofdeathErrorLabel->setText("<span style='color:red'>You can't select a year lower than year of birth</span>");
            return false;
        }
    }

    return true;
}

bool addItemsDialog::inventorAddValidate()
{
    inventorErrorLabelsClear();

    if(ui->inventorScientistsDropdown->currentIndex() == 0)
    {
        ui->inventorScientistsErrorLabel->setText("<span style='color:red'>Select a valid scientist</span>");
        return false;
    }
    if(ui->inventorComputersDropdown->currentIndex() == 0)
    {
        ui->inventorComputersErrorLabel->setText("<span style='color:red'>Select a valid computer</span>");
        return false;
    }

    return true;
}

//--- validation ends ---//


//--- add buttons start ---//
// behaviour when pushing add buttons //
void addItemsDialog::on_scientistAddButton_clicked()
{
    ui->scientistNotificationLabel->setText("");

    if(scientistAddValidate())
    {
        string name = ui->scientistNameInput->text().toStdString();
        string gender = ui->scientistGenderDropdown->currentText().toStdString();
        int born = ui->scientistYearofbirthDropdown->currentText().toInt();
        int died = 0;
        if(ui->scientistDeadCheck->isChecked())
        {
            died = ui->scientistYearofdeathDropdown->currentText().toInt();
        }
        Scientist person = Scientist(name, gender, born, died);

        scientistAct.add(person);

        setScientistDefault();

        ui->scientistNotificationLabel->setText("<span style='color:green'>Scientist added</span>");
    }
}

void addItemsDialog::on_computerAddButton_clicked()
{
    ui->computerNotificationLabel->setText("");

    if(computerAddValidate())
    {
        string name = ui->computerNameInput->text().toStdString();
        string type = ui->computerTypeDropdown->currentText().toStdString();
        bool wasbuilt = 0;
        int yearbuilt = 0;
        if(ui->computerWasbuiltCheck->isChecked())
        {
            wasbuilt = 1;
            yearbuilt = ui->computerYearbuiltDropdown->currentText().toInt();
        }

        Computer comp = Computer(name, yearbuilt, type, wasbuilt);

        computerAct.add(comp);

        setComputerDefault();

        ui->computerNotificationLabel->setText("<span style='color:green'>Computer added</span>");
    }
}

void addItemsDialog::on_inventorAddButton_clicked()
{
    ui->inventorNotificationLabel->setText("");

    if(inventorAddValidate())
    {
        string scientistName = ui->inventorScientistsDropdown->currentText().toStdString();
        string computerName = ui->inventorComputersDropdown->currentText().toStdString();

        relationAct.add(scientistName, computerName);
        setInventorDefault();

        ui->inventorNotificationLabel->setText("<span style='color:green'>Connection made</span>");
    }
}

//--- add buttons end ---//

//--- clear buttons start ---//
// resets all fields to default/empty state when pushing Clear button //
void addItemsDialog::on_computerClearButton_clicked()
{
    setComputerDefault();
    ui->computerNotificationLabel->setText("");
}

void addItemsDialog::on_scientistClearButton_clicked()
{
    setScientistDefault();
    ui->scientistNotificationLabel->setText("");
}

void addItemsDialog::on_inventorClearButton_clicked()
{
    setInventorDefault();
    ui->inventorNotificationLabel->setText("");
}
//--- clear buttons end ---//

//--- cancel buttons start ---//
// when pushing Close button window is closed //
void addItemsDialog::on_computerCancelButton_clicked()
{
    this->close();
}

void addItemsDialog::on_scientistCancelButton_clicked()
{
    this->close();
}

void addItemsDialog::on_inventorCancelButton_clicked()
{
    this->close();
}
//--- cancel buttons end ---//

//--- check enabling/disabling starts ---//
// checkbox toggling enable/disable on specific dropdowns //
void addItemsDialog::on_computerWasbuiltCheck_toggled(bool checked)
{
    if(checked)
    {
        ui->computerYearbuiltLabel->setEnabled(true);
        ui->computerYearbuiltDropdown->setEnabled(true);
        ui->computerYearbuiltErrorLabel->setEnabled(true);
    }
    else
    {
        ui->computerYearbuiltLabel->setEnabled(false);
        ui->computerYearbuiltDropdown->setEnabled(false);
        ui->computerYearbuiltErrorLabel->setEnabled(false);
    }

}

void addItemsDialog::on_scientistDeadCheck_toggled(bool checked)
{
    if(checked)
    {
        ui->scientistYearofdeathLabel->setEnabled(true);
        ui->scientistYearofdeathDropdown->setEnabled(true);
        ui->scientistYearofdeathErrorLabel->setEnabled(true);
    }
    else
    {
        ui->scientistYearofdeathLabel->setEnabled(false);
        ui->scientistYearofdeathDropdown->setEnabled(false);
        ui->scientistYearofdeathErrorLabel->setEnabled(false);

    }
}
//--- check enabling/disabling ends ---//

//--- clear error labels start ---//
// removes all messages in error labels //
void addItemsDialog::scientistErrorLabelsClear()
{
    ui->scientistNameErrorLabel->setText("");
    ui->scientistGenderErrorLabel->setText("");
    ui->scientistYearofbirthErrorLabel->setText("");
    ui->scientistDeadErrorLabel->setText("");
    ui->scientistYearofdeathErrorLabel->setText("");
}

void addItemsDialog::computerErrorLabelsClear()
{
    ui->computerNameErrorLabel->setText("");
    ui->computerTypeErrorLabel->setText("");
    ui->computerWasbuiltErrorLabel->setText("");
    ui->computerYearbuiltErrorLabel->setText("");
}

void addItemsDialog::inventorErrorLabelsClear()
{
    ui->inventorScientistsErrorLabel->setText("");
    ui->inventorComputersErrorLabel->setText("");
}
//--- clear error labels end ---//

//--- input from keyboard checks start ---//
// checks input as soon as user goes out of name input field //
void addItemsDialog::on_computerNameInput_editingFinished()
{
    computerNameInputValid();
}

void addItemsDialog::on_scientistNameInput_editingFinished()
{
    scientistNameInputValid();
}
//--- input from keyboard checks end ---//

//--- tabs special start ---//
// resets a tab to a default state when you click on it //
void addItemsDialog::on_addItemsTabs_tabBarClicked(int index)
{
    if(index == 0)
    {
        setScientistDefault();
        ui->scientistNotificationLabel->setText("");
        ui->scientistYearofbirthDropdown->clear();
        ui->scientistYearofdeathDropdown->clear();
        getScientistYearBorn();
        getScientistYearDied();

    }
    if(index == 1)
    {
        setComputerDefault();
        ui->computerNotificationLabel->setText("");
        ui->computerTypeDropdown->clear();
        ui->computerYearbuiltDropdown->clear();
        getComputerTypes();
        getComputerYearBuilt();
    }
    if(index == 2)
    {
        setInventorDefault();
        ui->inventorNotificationLabel->setText("");
        ui->inventorScientistsDropdown->clear();
        ui->inventorComputersDropdown->clear();
        getInvScientists();
        getInvComputers();
    }
}
//--- tabs special end ---//

//--- dropdown selections start ---//
list<QString> addItemsDialog::getYearSelection()
{
    QDate date = QDate::currentDate();
    list<QString> years = list<QString>();

    years.push_back("Select year");

    for(int i = 1750; i <= date.year(); i++)
    {
        years.push_back(QString::number(i));
    }

    return years;
}

void addItemsDialog::getComputerTypes()
{
    ui->computerTypeDropdown->setEnabled(true);
    ui->computerTypeDropdown->clear();
    ui->computerTypeDropdown->addItem("Select type");

    try {
        currentTypes = computerAct.getAllTypes();

        for(list<string>::iterator iter = currentTypes.begin(); iter != currentTypes.end(); iter++)
        {
            ui->computerTypeDropdown->addItem(QString::fromStdString(*iter));
        }
    } catch(NoSuchTableException e) {
        ui->computerTypeDropdown->setEnabled(false);
        ui->computerTypeErrorLabel->setText("<span style='color:red'>There's no table of Computer Types in the database.</span>");
    }
}

void addItemsDialog::getScientistYearBorn()
{
    yearSelection = getYearSelection();

    for(list<QString>::iterator iter = yearSelection.begin(); iter != yearSelection.end(); iter++)
    {
        ui->scientistYearofbirthDropdown->addItem(*iter);
    }
}

void addItemsDialog::getScientistYearDied()
{
    yearSelection = getYearSelection();

    for(list<QString>::iterator iter = yearSelection.begin(); iter != yearSelection.end(); iter++)
    {
        ui->scientistYearofdeathDropdown->addItem(*iter);
    }
}

void addItemsDialog::getComputerYearBuilt()
{
    yearSelection = getYearSelection();

    for(list<QString>::iterator iter = yearSelection.begin(); iter != yearSelection.end(); iter++)
    {
        ui->computerYearbuiltDropdown->addItem(*iter);
    }
}

void addItemsDialog::getInvScientists()
{
    ui->inventorScientistsDropdown->clear();
    ui->inventorScientistsDropdown->addItem("Select a scientist");

    try {
        currentScientists = scientistAct.getAllScientists();

        for(list<Scientist>::iterator iter = currentScientists.begin(); iter != currentScientists.end(); iter++)
        {
            ui->inventorScientistsDropdown->addItem(QString::fromStdString(iter->getName()));
        }
    } catch(NoSuchTableException e) {
        ui->inventorScientistsDropdown->setEnabled(false);
        ui->inventorScientistsErrorLabel->setText("<span style='color:red'>There's no table of Scientists in the database.</span>");
    }
}

void addItemsDialog::getInvComputers()
{
    ui->inventorComputersDropdown->clear();
    ui->inventorComputersDropdown->addItem("Select a computer");

    try {
        currentComputers = computerAct.getAllComputers();

        for(list<Computer>::iterator iter = currentComputers.begin(); iter != currentComputers.end(); iter++)
        {
            ui->inventorComputersDropdown->addItem(QString::fromStdString(iter->getName()));
        }
    } catch(NoSuchTableException e) {
        ui->inventorComputersDropdown->setEnabled(false);
        ui->inventorComputersErrorLabel->setText("<span style='color:red'>There's no table of Computers in the database.</span>");
    }
}
//--- dropdown selections end ---//

//--- add a computer type start ---//
// to open the add computer type window //
void addItemsDialog::on_computerTypeAddButton_clicked()
{
    // used to decide whether or not to print a success message or not
    currentTypes = computerAct.getAllTypes();
    int currentSize = currentTypes.size();

    addComputerTypeDialog addType;
    addType.exec();

    on_addItemsTabs_tabBarClicked(1);

    // continued for decision of printing a success message
    int newSize = currentTypes.size();
    if(currentSize < newSize)
    {
        ui->computerNotificationLabel->setText("<span style='color:green'>Successfully added a new type</span>");
    }

    //ui->computerTypeErrorLabel->setText("");
    //getComputerTypes();
}
//--- add a computer type end ---//

//--- return fields to default state start ---//
// resets specific fields to a default/empty state //
void addItemsDialog::setScientistDefault()
{
    scientistErrorLabelsClear();
    ui->scientistNameInput->clear();
    ui->scientistGenderDropdown->setCurrentIndex(0);
    ui->scientistYearofbirthDropdown->setCurrentIndex(0);
    ui->scientistDeadCheck->setChecked(false);
    ui->scientistYearofdeathDropdown->setCurrentIndex(0);
}

void addItemsDialog::setComputerDefault()
{
    computerErrorLabelsClear();
    ui->computerNameInput->clear();
    ui->computerTypeDropdown->setCurrentIndex(0);
    ui->computerWasbuiltCheck->setChecked(false);
    ui->computerYearbuiltDropdown->setCurrentIndex(0);
}

void addItemsDialog::setInventorDefault()
{
    inventorErrorLabelsClear();
    ui->inventorScientistsDropdown->setCurrentIndex(0);
    ui->inventorComputersDropdown->setCurrentIndex(0);
}

//--- return fields to default state end ---//
