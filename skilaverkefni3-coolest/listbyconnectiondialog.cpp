#include "listbyconnectiondialog.h"
#include "ui_listbyconnectiondialog.h"

ListByConnectionDialog::ListByConnectionDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ListByConnectionDialog) {

    ui->setupUi(this);

    ui->label_NoRelationsToDisplay->setText("");
    ui->label_NoRelationsToDisplay2->setText("");

    try {
        getConnectionInfo();
        if(currentRelations.size() != 0) {
            fillDropDownLists();
        }
        else {
            ui->dropDownOfScientists->setEnabled(false);
            ui->table_listOfComputers->setEnabled(false);
            ui->label_NoRelationsToDisplay->setText("<span style='color:red'>There're no Connections between a scientist and a computer.</span>");
            ui->dropDownOfComputers->setEnabled(false);
            ui->table_listOfScientists->setEnabled(false);
            ui->label_NoRelationsToDisplay2->setText("<span style='color:red'>There're no Connections between a scientist and a computer.</span>");
        }
    } catch(NoSuchTableException e) {
        ui->dropDownOfScientists->setEnabled(false);
        ui->table_listOfComputers->setEnabled(false);
        ui->label_NoRelationsToDisplay->setText("<span style='color:red'>There's no Relations table in the database.</span>");
        ui->dropDownOfComputers->setEnabled(false);
        ui->table_listOfScientists->setEnabled(false);
        ui->label_NoRelationsToDisplay2->setText("<span style='color:red'>There's no Relations table in the database.</span>");
    }
}

ListByConnectionDialog::~ListByConnectionDialog() {
    delete ui;
}

void ListByConnectionDialog::getConnectionInfo() {
    currentRelations = rel_actions.getAllRelations();
    allCurrentlyConnectedScientists = rel_actions.listOfAllConnectedScientists();
    allCurrentlyConnectedComputers = rel_actions.listOfAllConnectedComputers();
}

void ListByConnectionDialog::fillDropDownLists() {
    ui->dropDownOfScientists->addItem("Select a Scientist...");
    for(list<string>::iterator iter = allCurrentlyConnectedScientists.begin();
        iter != allCurrentlyConnectedScientists.end(); iter++) {
        ui->dropDownOfScientists->addItem(QString::fromStdString(*iter));
    }
    ui->dropDownOfComputers->addItem("Select a Computer...");
    for(list<string>::iterator iter = allCurrentlyConnectedComputers.begin();
        iter != allCurrentlyConnectedComputers.end(); iter++) {
        ui->dropDownOfComputers->addItem(QString::fromStdString(*iter));
    }
}

void ListByConnectionDialog::displayScientistsRelation(const QString &sName) {
    ui->table_listOfComputers->clearContents();
    ui->table_listOfComputers->setColumnWidth(0,150);
    ui->table_listOfComputers->setColumnWidth(1,100);
    ui->table_listOfComputers->setColumnWidth(2,100);
    ui->table_listOfComputers->setColumnWidth(3,200);
    ui->table_listOfComputers->setRowCount(currentRelations.size());

    currentlyDisplayedComputers.clear();

    for(list<Relation>::iterator iter = currentRelations.begin(); iter != currentRelations.end(); iter++) {
        if(iter->isItThisScientist(sName.toStdString())) {
            QString name = QString::fromStdString(iter->getComputerName());
            int yb = iter->getComputerYearBuilt();
            QString ybuilt = "";
            if(yb != 0) {
                ybuilt = QString::fromStdString(iter->ybToString());
            }
            QString type = QString::fromStdString(iter->getComputerType());
            bool wb = iter->getComputerWasBuilt();
            QString wasBuilt = "Yes";
            if(!wb) {
                wasBuilt = "No";
            }

            int currentRow = currentlyDisplayedComputers.size();

            ui->table_listOfComputers->setItem(currentRow, 0, new QTableWidgetItem(name));
            ui->table_listOfComputers->setItem(currentRow, 1, new QTableWidgetItem(wasBuilt));
            ui->table_listOfComputers->item(currentRow, 1)->setTextAlignment(Qt::AlignHCenter|Qt::AlignVCenter);
            ui->table_listOfComputers->setItem(currentRow, 2, new QTableWidgetItem(ybuilt));
            ui->table_listOfComputers->item(currentRow, 2)->setTextAlignment(Qt::AlignHCenter|Qt::AlignVCenter);
            ui->table_listOfComputers->setItem(currentRow, 3, new QTableWidgetItem(type));

            currentlyDisplayedComputers.push_back(*iter);
        }
    }
}

void ListByConnectionDialog::displayComputersRelation(const QString &cName) {
    ui->table_listOfScientists->clearContents();
    ui->table_listOfScientists->setColumnWidth(0,200);
    ui->table_listOfScientists->setColumnWidth(1,100);
    ui->table_listOfScientists->setColumnWidth(2,100);
    ui->table_listOfScientists->setColumnWidth(3,100);
    ui->table_listOfScientists->setRowCount(currentRelations.size());

    currentlyDisplayedScientists.clear();

    for(list<Relation>::iterator iter = currentRelations.begin(); iter != currentRelations.end(); iter++) {
        if(iter->isItThisComputer(cName.toStdString())) {
            QString name = QString::fromStdString(iter->getScientistName());
            QString gender = QString::fromStdString(iter->getScientistGender());
            QString yob = QString::fromStdString(iter->yobToString());
            int yod = iter->getScientistYearOfDeath();
            QString yofdeath = "";
            if(yod != 0) {
                yofdeath = QString::fromStdString(iter->yodToString());
            } else {}

            int currentRow = currentlyDisplayedScientists.size();

            ui->table_listOfScientists->setItem(currentRow, 0, new QTableWidgetItem(name));
            ui->table_listOfScientists->setItem(currentRow, 1, new QTableWidgetItem(gender));
            ui->table_listOfScientists->setItem(currentRow, 2, new QTableWidgetItem(yob));
            ui->table_listOfScientists->item(currentRow, 2)->setTextAlignment(Qt::AlignHCenter|Qt::AlignVCenter);
            ui->table_listOfScientists->setItem(currentRow, 3, new QTableWidgetItem(yofdeath));
            ui->table_listOfScientists->item(currentRow, 3)->setTextAlignment(Qt::AlignHCenter|Qt::AlignVCenter);

            currentlyDisplayedScientists.push_back(*iter);
        }
    }
}

void ListByConnectionDialog::on_dropDownOfScientists_activated(const QString &arg1) {
    displayScientistsRelation(arg1);
}

void ListByConnectionDialog::on_dropDownOfComputers_activated(const QString &arg1) {
    displayComputersRelation(arg1);
}

void ListByConnectionDialog::on_table_listOfComputers_doubleClicked(const QModelIndex &index) {
    int rowNumber = index.row();
    string strToGoogle = index.sibling(rowNumber, 0).data().toString().toStdString();
    util.searchGoogle(strToGoogle);
}

void ListByConnectionDialog::on_table_listOfScientists_doubleClicked(const QModelIndex &index) {
    int rowNumber = index.row();
    string strToGoogle = index.sibling(rowNumber, 0).data().toString().toStdString();
    util.searchGoogle(strToGoogle);
}
