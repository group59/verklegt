#ifndef DATABASEINIT_H
#define DATABASEINIT_H

#include "datasqlaccess.h"

class DatabaseInit {

    public:
        bool start();
        void quit();
    private:
        DataSqlAccess dataAccess;
};

#endif // DATABASEINIT_H
