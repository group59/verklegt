#ifndef ADDCOMPUTERTYPEDIALOG_H
#define ADDCOMPUTERTYPEDIALOG_H

#include <QDialog>
#include "additemsdialog.h"
#include "Domain-Actions/domaincomputeractions.h"

// this is used to add a new computer type to a computer type table
// that is then used to populate a dropdown which is used to add a new computer

namespace Ui {
class addComputerTypeDialog;
}

class addComputerTypeDialog : public QDialog
{
    Q_OBJECT

public:
    explicit addComputerTypeDialog(QWidget *parent = 0);
    ~addComputerTypeDialog();

private slots:
    void on_addComputerTypeCancelButton_clicked();
    void on_addComputerTypeAddButton_clicked();

private:
    Ui::addComputerTypeDialog *ui;

    DomainComputerActions comAct;
    list<string> comType;

    bool nameValidation();
};

#endif // ADDCOMPUTERTYPEDIALOG_H
