#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow) {

    ui->setupUi(this);

    if(!init.start()) {
        exit(1);
    }
}

MainWindow::~MainWindow() {
    init.quit();
    delete ui;
}

void MainWindow::on_pushButton_ListAndSearch_pressed() {
    ListAndSearchDialog listAndSearch;
    listAndSearch.exec();
}

void MainWindow::on_pushButton_Exit_pressed() {
    ExitDialog exiting;
    if(exiting.exec()) {
        this->close();
    }
}

void MainWindow::on_pushButton_ListByConnection_pressed() {
    ListByConnectionDialog listByCon;
    listByCon.exec();
}

void MainWindow::on_pushButton_Add_pressed() {
    addItemsDialog addItems;
    addItems.exec();
}

void MainWindow::on_button_About_triggered() {
    aboutDialog = new AboutDialog(this);
    aboutDialog->show();
}
