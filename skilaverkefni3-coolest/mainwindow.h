#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "databaseinit.h"
#include "aboutdialog.h"
#include "additemsdialog.h"
#include "listandsearchdialog.h"
#include "listbyconnectiondialog.h"
#include "exitdialog.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pushButton_ListAndSearch_pressed();
    void on_pushButton_Exit_pressed();
    void on_pushButton_ListByConnection_pressed();
    void on_pushButton_Add_pressed();
    void on_button_About_triggered();

private:
    Ui::MainWindow *ui;
    DatabaseInit init;
    AboutDialog* aboutDialog;
};

#endif // MAINWINDOW_H
