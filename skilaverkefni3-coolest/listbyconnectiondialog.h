#ifndef LISTBYCONNECTIONDIALOG_H
#define LISTBYCONNECTIONDIALOG_H

#include <QDialog>
#include <string>
#include "Containers/relation.h"
#include "utilities.h"
#include "Domain-Actions/domainrelationsactions.h"

namespace Ui {
class ListByConnectionDialog;
}

class ListByConnectionDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ListByConnectionDialog(QWidget *parent = 0);
    ~ListByConnectionDialog();

private slots:
    void on_dropDownOfScientists_activated(const QString &arg1);
    void on_dropDownOfComputers_activated(const QString &arg1);
    void on_table_listOfComputers_doubleClicked(const QModelIndex &index);
    void on_table_listOfScientists_doubleClicked(const QModelIndex &index);

private:
    Ui::ListByConnectionDialog *ui;

    DomainRelationsActions rel_actions;
    Utilities util;

    list<Relation> currentRelations;
    list<Relation> currentlyDisplayedScientists;
    list<Relation> currentlyDisplayedComputers;
    list<string> allCurrentlyConnectedScientists;
    list<string> allCurrentlyConnectedComputers;

    void getConnectionInfo();
    void fillDropDownLists();
    void displayScientistsRelation(const QString &sName);
    void displayComputersRelation(const QString &cName);
};

#endif // LISTBYCONNECTIONDIALOG_H
