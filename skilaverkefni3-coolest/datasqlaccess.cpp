#include "datasqlaccess.h"

// handles all interactions and connections to the database (or database file)

DataSqlAccess::DataSqlAccess()
{
    connectionName = "verklegt";
}

// connects to a database if it exists, otherwise tries to create it
void DataSqlAccess::connect()
{
    database = QSqlDatabase::addDatabase("QSQLITE", connectionName);
    database.setDatabaseName("verklegt-db.sqlite");

    database.open();
}

// disconnects link to a database (unless it has already been closed)
void DataSqlAccess::disconnect()
{
    if(database.isOpen())
    {
        database.close();
    }
}

// checks if database exists
bool DataSqlAccess::databaseExists()
{
    QFileInfo info1("verklegt-db.sqlite");
    if(info1.exists()) {
        return true;
    }
    else {
        return false;
    }
}

// checks if link to database is open and returns the connection
// safeguard if connection to database goes down for some reason
QSqlDatabase DataSqlAccess::getDatabaseConnection()
{
    QSqlDatabase db;

    if(QSqlDatabase::contains(connectionName))
    {
        db = QSqlDatabase::database(connectionName);
    }
    else
    {
        db = QSqlDatabase::addDatabase("QSQLITE", connectionName);
        db.setDatabaseName("verklegt-db.sqlite");

        db.open();
    }

    return db;
}
