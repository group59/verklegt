#ifndef EXITDIALOG_H
#define EXITDIALOG_H

#include <QDialog>
#include "mainwindow.h"

namespace Ui {
class ExitDialog;
}

class ExitDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ExitDialog(QWidget *parent = 0);
    ~ExitDialog();

private slots:
    bool on_buttonBox_ToExit_accepted();
    bool on_buttonBox_ToExit_rejected();

private:
    Ui::ExitDialog *ui;
};

#endif // EXITDIALOG_H
