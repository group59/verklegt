#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication skilaverkefni3App(argc, argv);

    MainWindow window;
    window.show();

    return skilaverkefni3App.exec();
}
