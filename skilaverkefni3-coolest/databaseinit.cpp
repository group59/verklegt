#include "databaseinit.h"
#include "createdatabasedialog.h"

// starts a connection to the database, supposed to keep it open until program ends

bool DatabaseInit::start()
{
    if(!dataAccess.databaseExists()) {
        CreateDatabaseDialog createYorN;
        if(createYorN.exec()) {
            dataAccess.connect();
        }
        else {
            return false;
        }
    }
    else {
        dataAccess.connect();
    }

    return true;
}

void DatabaseInit::quit() {
    dataAccess.disconnect();
}
