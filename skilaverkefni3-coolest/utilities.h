#ifndef UTILITIES_H
#define UTILITIES_H

#include <string>
#include <algorithm>
using namespace std;

class Utilities {
    public:
        string stringToLower(string original);      // changes all upper chars to lower for searching purposes
        void searchGoogle(string stringToSearch);   // takes in a string to search, uses *searchstring to add prefix
                                                    // to it and change it to a character pointer and then starts up
                                                    // chrome and searches for a Scientists or Computers name.
        char *searchstring(string toChange);        // adds the prefix and changes string to character pointer
};

class NoSuchTableException {  // used in the data layer or repositories
};

#endif // UTILITIES_H
