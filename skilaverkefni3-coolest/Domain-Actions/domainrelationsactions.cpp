#include "Domain-Actions/domainrelationsactions.h"

DomainRelationsActions::DomainRelationsActions() {
    rrepo = RelationRepo();
}

void DomainRelationsActions::add(string first, string second) {
    rrepo.findIDs(first, second);
}

list<Relation> DomainRelationsActions::getAllRelations() {
    list<Relation> r = rrepo.getAllRelations();
    return r;
}

list<string> DomainRelationsActions::listOfAllConnectedScientists()
{
    return rrepo.listOfAllConnectedScientists();
}

list<string> DomainRelationsActions::listOfAllConnectedComputers()
{
    return rrepo.listOfAllConnectedComputers();
}
