#ifndef DOMAINCOMPUTERACTIONS_H
#define DOMAINCOMPUTERACTIONS_H

#include <list>
#include <string>
#include <cstdlib>
#include <stdexcept>
#include "Repositories/computerrepo.h"
using namespace std;

class DomainComputerActions {

private:
    ComputerRepo crepo;
public:
    DomainComputerActions();           // constructor initiates an instance of PeopleRepo

    void add(Computer c);
    void addType(string name);

    list<Computer> getAllComputers();
    list<string> getAllTypes();
};

#endif // DOMAINCOMPUTERACTIONS_H
