#ifndef DOMAINSCIENTISTSACTIONS_H
#define DOMAINSCIENTISTSACTIONS_H

#include <list>
#include <string>
#include <cstdlib>
#include <stdexcept>
#include "Repositories/scientistrepo.h"
using namespace std;

class DomainScientistsActions {

private:
    ScientistRepo scirepo;
public:
    DomainScientistsActions();           // constructor initiates an instance of PeopleRepo

    void add(Scientist s);

    list<Scientist> getAllScientists();
};

#endif // DOMAINSCIENTISTSACTIONS_H
