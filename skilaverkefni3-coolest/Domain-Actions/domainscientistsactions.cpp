#include "Domain-Actions/domainscientistsactions.h"

DomainScientistsActions::DomainScientistsActions() {
    scirepo = ScientistRepo();
}

//adds a new person and information about it to the database
//if the person is still alive, then yod is space
void DomainScientistsActions::add(Scientist s) {
    scirepo.add(s);
}

list<Scientist> DomainScientistsActions::getAllScientists() {
    list<Scientist> s = scirepo.getAllScientists();
    return s;
}
