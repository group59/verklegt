#include "Domain-Actions/domaincomputeractions.h"

DomainComputerActions::DomainComputerActions() {
    crepo = ComputerRepo();
}

//adds new computer and information about it to the database
void DomainComputerActions::add(Computer c) {
    crepo.add(c);
}

void DomainComputerActions::addType(string name) {
    crepo.addType(name);
}

list<Computer> DomainComputerActions::getAllComputers() {
    list<Computer> allComputers = crepo.getAllComputers();
    return allComputers;
}

list<string> DomainComputerActions::getAllTypes() {
    list<string> allTypes = crepo.getAllTypes();
    return allTypes;
}
