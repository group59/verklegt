#ifndef DOMAINRELATIONSACTIONS_H
#define DOMAINRELATIONSACTIONS_H

#include <list>
#include <string>
#include <cstdlib>
#include <stdexcept>
#include "Repositories/relationrepo.h"
using namespace std;

class DomainRelationsActions {

private:
    RelationRepo rrepo;
public:
    DomainRelationsActions();           // constructor initiates an instance of PeopleRepo
    void add(string first, string second);
    list<Relation> getAllRelations();
    list<string> listOfAllConnectedScientists();
    list<string> listOfAllConnectedComputers();
};

#endif // DOMAINRELATIONSACTIONS_H
