#include "Repositories/computerrepo.h"

ComputerRepo::ComputerRepo()
{
    computerData = DataSqlAccess();
    tableName = "Computers";
    tableTypeName = "CompTypes";
    computerDB = computerData.getDatabaseConnection();
}

// checks if the table for this repo exists
bool ComputerRepo::tableExists() {
    if(computerDB.tables().contains(tableName)) {
        return true;
    }
    else {
        return false;
    }
}

bool ComputerRepo::tableExistsForTypes() {
    if(computerDB.tables().contains(tableTypeName)) {
        return true;
    }
    else {
        return false;
    }
}

// creates a table, writes to qdebug if not successful
void ComputerRepo::createTable()
{
    QSqlQuery query(computerDB);

    if(!query.exec("CREATE TABLE "+tableName+"(ID INTEGER PRIMARY KEY AUTOINCREMENT, Name VARCHAR NOT NULL, YearBuilt INTEGER, Type VARCHAR NOT NULL, WasBuilt BOOL);"))
    {
        qDebug() << query.lastError();
    }
}

// creates a table for Computer Types, writes to qdebug if not successful
void ComputerRepo::createTypeTable()
{
    QSqlQuery query(computerDB);

    if(!query.exec("CREATE TABLE "+tableTypeName+"(ID INTEGER PRIMARY KEY AUTOINCREMENT, TypeName VARCHAR NOT NULL UNIQUE);"))
    {
        qDebug() << query.lastError();
    }
}

// adds a computer to the Computers table in the database
void ComputerRepo::add(Computer c)
{    
    if(!tableExists())
    {
        createTable();
    }

    QSqlQuery query(computerDB);

    query.prepare("INSERT INTO " +tableName+ "(Name, YearBuilt, Type, WasBuilt)"
                  "VALUES(:name, :yearbuilt, :type, :wasbuilt);");
    query.bindValue(":name", QString::fromStdString(c.getName()));
    query.bindValue(":yearbuilt", c.getYearBuilt());
    query.bindValue(":type", QString::fromStdString(c.getType()));
    query.bindValue(":wasbuilt", c.getWasBuilt());

    if(!query.exec()) {
        qDebug() << query.lastError();
    }
}

void ComputerRepo::addType(string typeName) {
    if(!tableExistsForTypes()) {
        createTypeTable();
    }

    QSqlQuery query(computerDB);

    query.prepare("INSERT INTO " +tableTypeName+ "(TypeName) VALUES(:name);");
    query.bindValue(":name", QString::fromStdString(typeName));

    if(!query.exec()) {
        qDebug() << query.lastError();
    }
}

// creates a list of all the computers to be returned
list<Computer> ComputerRepo::getAllComputers()
{
    QSqlQuery query(computerDB);

    if(!query.exec("SELECT * FROM " +tableName+ " ORDER BY Name")) {
        throw NoSuchTableException();
    }

    list<Computer> allComputers = list<Computer>();

    while(query.next()) {
        string name, type;
        int YB;
        bool WB;
        name = query.value("Name").toString().toStdString();
        YB = query.value("YearBuilt").toInt();
        type = query.value("Type").toString().toStdString();
        WB = query.value("WasBuilt").toBool();

        Computer c = Computer(name, YB, type, WB);
        allComputers.push_back(c);
    }

    return allComputers;
}

list<string> ComputerRepo::getAllTypes() {
    QSqlQuery query(computerDB);

    if(!query.exec("SELECT * FROM " +tableTypeName)) {
        throw NoSuchTableException();
    }

    list<string> allTypes = list<string>();

    while(query.next()) {
        string currTypeName = query.value("TypeName").toString().toStdString();
        allTypes.push_back(currTypeName);
    }

    return allTypes;
}
