#ifndef SCIENTISTREPO_H
#define SCIENTISTREPO_H

#include "datasqlaccess.h"
#include "utilities.h"
#include "Containers/scientist.h"

// handles all database interactions regarding Persons or Scientists
class ScientistRepo {
    public:
        ScientistRepo();
        void add(Scientist s);
        list<Scientist> getAllScientists();
        bool tableExists();
    private:
        DataSqlAccess scientistData;
        QString tableName;
        QSqlDatabase personDB;
        void createTable();
};

#endif // SCIENTISTREPO_H
