#ifndef RELATIONREPO_H
#define RELATIONREPO_H

#include "datasqlaccess.h"
#include "utilities.h"
#include "Containers/scientist.h"
#include "Containers/computer.h"
#include "Containers/relation.h"

// handles interaction with the relations table in database
// relation table is the many-to-many relation table

class RelationRepo {
    public:
        RelationRepo();
        bool tableExists();
        void add(int personID, int computerID);
        list<Relation> getAllRelations();
        void findIDs(string person, string computer);
        list<string> listOfAllConnectedScientists();
        list<string> listOfAllConnectedComputers();
    private:
        DataSqlAccess relationData;
        QString tableName;
        QSqlDatabase relationDB;
        void createTable();
};

#endif // RELATIONREPO_H
