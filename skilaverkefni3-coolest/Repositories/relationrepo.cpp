#include "Repositories/relationrepo.h"

RelationRepo::RelationRepo()
{
    relationData = DataSqlAccess();
    tableName = "Relations";
    relationDB = relationData.getDatabaseConnection();
}

//if the table exists, returns true, else return false.
//if false is returned, the table (query) is created

bool RelationRepo::tableExists()
{
    if(relationDB.tables().contains(tableName))
    {
        return true;
    }
    else
    {
        return false;
    }
}

// creates a table in the database for this repo
void RelationRepo::createTable()
{
    QSqlQuery query(relationDB);

    if(!query.exec("CREATE TABLE "+tableName+"(SciID INTEGER, ComID INTEGER, "
                   "FOREIGN KEY (SciID) REFERENCES Scientists(ID), "
                   "FOREIGN KEY (ComID) REFERENCES Computers(ID), "
                   "PRIMARY KEY (SciID, ComID));"))
    {
        qDebug() << query.lastError();
    }
}

// take in two strings based on user input, UI sanitises input so we are
// sure that only one of each string exists when they are sent to this function
// we then find the ID based on the strings, change those to int
// those integer values are then pushed into the add function
void RelationRepo::findIDs(string person, string computer)
{
    QString personTemp = QString::fromStdString(person);
    QString computerTemp = QString::fromStdString(computer);

    QSqlQuery queryPerson(relationDB);
    QSqlQuery queryComputer(relationDB);

    int personID;
    int computerID;

    if(!queryPerson.exec("SELECT ID FROM Scientists s WHERE s.Name LIKE '%"+personTemp+"%'"))
    {
        qDebug() << queryPerson.lastError();
    }

    if(!queryComputer.exec("SELECT ID FROM Computers c WHERE c.Name LIKE '%"+computerTemp+"%'"))
    {
        qDebug() << queryComputer.lastError();
    }

    queryPerson.next();
    queryComputer.next();
    personID = queryPerson.value("ID").toInt();
    computerID = queryComputer.value("ID").toInt();

    add(personID, computerID);

}

// adds a database entry based on integers received
void RelationRepo::add(int personID, int computerID)
{
    if(!tableExists())
    {
        createTable();
    }

    QSqlQuery query(relationDB);

    query.prepare("INSERT INTO Relations(SciID, ComID) VALUES (:sciid, :comid)");
    query.bindValue(":sciid", personID);
    query.bindValue(":comid", computerID);

    if(!query.exec())
    {
        qDebug() << query.lastError();
    }
}

// prints out a raw list of every connection, not sorted
list<Relation> RelationRepo::getAllRelations()
{
    QSqlQuery query(relationDB);

    if(!query.exec("SELECT r.SciID AS 'sID', s.Name AS 'sName', s.Gender AS 'sGender',"
                   " s.Born AS 'sYOB', s.Died AS 'sYOD', r.ComID AS 'cID',"
                   " c.Name AS 'cName', c.Type AS 'cType', c.YearBuilt AS 'cYB',"
                   " c.WasBuilt AS 'cWB' FROM Scientists s"
                   " INNER JOIN Relations r"
                   " ON r.SciID = s.ID"
                   " INNER JOIN Computers c"
                   " ON r.ComID = c.ID"
                   " ORDER BY s.Name"))
    {
        throw NoSuchTableException();
    }

    list<Relation> allRelations = list<Relation>();

    while(query.next())
    {
        int sID, sYOB, sYOD, cID, cYB;
        string sName, sGender, cName, cType;
        bool cWasBuilt;

        sID = query.value("sID").toInt();
        sName = query.value("sName").toString().toStdString();
        sGender = query.value("sGender").toString().toStdString();
        sYOB = query.value("sYOB").toInt();
        sYOD = query.value("sYOD").toInt();
        cID = query.value("cID").toInt();
        cName = query.value("cName").toString().toStdString();
        cType = query.value("cType").toString().toStdString();
        cYB = query.value("cYB").toInt();
        cWasBuilt = query.value("cWB").toBool();

        Relation r = Relation(sID, sName, sGender, sYOB, sYOD, cID, cName, cType, cWasBuilt, cYB);
        allRelations.push_back(r);
    }

    return allRelations;
}

list<string> RelationRepo::listOfAllConnectedScientists()
{
    QSqlQuery query(relationDB);
    if(!query.exec("SELECT DISTINCT r.SciID AS 'sID', s.Name AS 'Name'"
                   " FROM Relations r, Scientists s"
                   " WHERE s.ID = r.SciID"
                   " ORDER BY s.Name"))
    {
        qDebug() << query.lastError();
    }

    list<string> allScientists = list<string>();

    while(query.next())
    {
        string sciName = query.value("Name").toString().toStdString();

        allScientists.push_back(sciName);
    }

    return allScientists;
}

list<string> RelationRepo::listOfAllConnectedComputers()
{
    QSqlQuery query(relationDB);
    if(!query.exec("SELECT DISTINCT r.ComID AS 'cID', c.Name AS 'Name'"
                   " FROM Relations r, Computers c"
                   " WHERE c.ID = r.ComID"
                   " ORDER BY c.Name"))
    {
        qDebug() << query.lastError();
    }

    list<string> allComputers = list<string>();

    while(query.next())
    {
        string comName = query.value("Name").toString().toStdString();

        allComputers.push_back(comName);
    }

    return allComputers;
}

