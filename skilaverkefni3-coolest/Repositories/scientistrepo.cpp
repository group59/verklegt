#include "Repositories/scientistrepo.h"

ScientistRepo::ScientistRepo()
{
    scientistData = DataSqlAccess();
    tableName = "Scientists";
    personDB = scientistData.getDatabaseConnection();
}

// checks if a table with tablename exists in the database
bool ScientistRepo::tableExists()
{
    if(personDB.tables().contains(tableName))
    {
        return true;
    }
    else
    {
        return false;
    }
}

// creates a table with tablename, if query command fails writes out to qdebug
void ScientistRepo::createTable()
{
    QSqlQuery query(personDB);

    if(!query.exec("CREATE TABLE "+tableName+"(ID INTEGER PRIMARY KEY AUTOINCREMENT, Name VARCHAR NOT NULL, Gender VARCHAR NOT NULL, Born INTEGER NOT NULL, Died INTEGER);"))
    {
        qDebug() << query.lastError();
    }
}

// adds a person to the Scientists table in the database
// calls for table creation if it doesn't exist
void ScientistRepo::add(Scientist s)
{
    if(!tableExists())
    {
        createTable();
    }

    QSqlQuery query(personDB);

    query.prepare("INSERT INTO Scientists (Name, Gender, Born, Died)"
                  "VALUES(:name, :gender, :born, :died);");
    query.bindValue(":name", QString::fromStdString(s.getName()));
    query.bindValue(":gender", QString::fromStdString(s.getGender()));
    query.bindValue(":born", s.getYOB());
    query.bindValue(":died", s.getYOD());

    if(!query.exec())
    {
        qDebug() << query.lastError();
    }

}

// takes in a string to determine what kind of call to table should be done
// then adds results from table into a list, returns that list
list<Scientist> ScientistRepo::getAllScientists() {
    QSqlQuery query(personDB);

    if(!query.exec("SELECT * FROM "+tableName+" ORDER BY Name")) {
        throw NoSuchTableException();
    }

    list<Scientist> allScientists = list<Scientist>();

    while(query.next())
    {
        string name, gender;
        int YOB, YOD;
        name = query.value("Name").toString().toStdString();
        gender = query.value("Gender").toString().toStdString();
        YOB = query.value("Born").toInt();
        YOD = query.value("Died").toInt();

        Scientist s = Scientist(name, gender, YOB, YOD);
        allScientists.push_back(s);
    }
    return allScientists;
}
