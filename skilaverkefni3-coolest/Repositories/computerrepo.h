#ifndef COMPUTERREPO_H
#define COMPUTERREPO_H

#include "datasqlaccess.h"
#include "utilities.h"
#include "Containers/computer.h"

// handles all database interactions regarding computers
class ComputerRepo {

    public:
        ComputerRepo();
        void add(Computer c);               // adds a computer to the database
        void addType(string typeName);      // adds a computer type to the database
        list<Computer> getAllComputers();   // returns a list of all the computers in the database
        list<string> getAllTypes();         // return a list of all the computer types in the database
        bool tableExists();                 // used to check if the Computers table exists in the DB
        bool tableExistsForTypes();         // used to check if the CompTypes table exists in the DB
    private:
        DataSqlAccess computerData;         // used to check if connection to the DB exists and to fetch
                                            // it if needed.
        QString tableName, tableTypeName;   // stores the tablenames that this repository works with
        QSqlDatabase computerDB;            // this repo's access to the database
        void createTable();                 // used to create the Computers table if it doesn't exist
        void createTypeTable();             // used to create the CompTypes table if it doesn't exist
};

#endif // COMPUTERREPO_H
