#ifndef CREATEDATABASEDIALOG_H
#define CREATEDATABASEDIALOG_H

#include <QDialog>

namespace Ui {
class CreateDatabaseDialog;
}

class CreateDatabaseDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CreateDatabaseDialog(QWidget *parent = 0);
    ~CreateDatabaseDialog();

private slots:
    bool on_buttonBox_accepted();
    bool on_buttonBox_rejected();

private:
    Ui::CreateDatabaseDialog *ui;
};

#endif // CREATEDATABASEDIALOG_H
