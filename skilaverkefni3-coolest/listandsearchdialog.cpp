#include "listandsearchdialog.h"
#include "ui_listandsearchdialog.h"

ListAndSearchDialog::ListAndSearchDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ListAndSearchDialog) {

    ui->setupUi(this);

    ui->label_NoScientistsInDatabase->setText("");
    ui->label_NoComputersInDatabase->setText("");
    ui->lineEdit_inputSciForSearch->setPlaceholderText("Search scientists...");
    ui->lineEdit_inputCompForSearch->setPlaceholderText("Search computers...");

    try {
        getAllScientists();
        if(currentScientists.size() == 0) {
            ui->lineEdit_inputSciForSearch->setEnabled(false);
            ui->table_listOfScientists->setEnabled(false);
            ui->label_NoScientistsInDatabase->setText("<span style='color:red'>There're no Scientists in the Database.</span>");
        }
    } catch(NoSuchTableException e) {
        ui->lineEdit_inputSciForSearch->setEnabled(false);
        ui->table_listOfScientists->setEnabled(false);
        ui->label_NoScientistsInDatabase->setText("<span style='color:red'>There's no table of Scientists in the database.</span>");
    }

    try {
        getAllComputers();
        if(currentComputers.size() == 0) {
            ui->lineEdit_inputCompForSearch->setEnabled(false);
            ui->table_listOfComputers->setEnabled(false);
            ui->label_NoComputersInDatabase->setText("<span style='color:red'>There're no Computers in the Database.</span>");
        }
    } catch(...) {
        ui->lineEdit_inputCompForSearch->setEnabled(false);
        ui->table_listOfComputers->setEnabled(false);
        ui->label_NoComputersInDatabase->setText("<span style='color:red'>There's no table of Computers in the database.</span>");
    }
}

ListAndSearchDialog::~ListAndSearchDialog() {
    delete ui;
}

void ListAndSearchDialog::getAllScientists() {
    currentScientists = sci_actions.getAllScientists();
    displayAllScientists();
}


void ListAndSearchDialog::displayAllScientists() {  
    ui->table_listOfScientists->clearContents();
    ui->table_listOfScientists->setColumnWidth(0,200);
    ui->table_listOfScientists->setColumnWidth(1,100);
    ui->table_listOfScientists->setColumnWidth(2,100);
    ui->table_listOfScientists->setColumnWidth(3,100);
    ui->table_listOfScientists->setRowCount(currentScientists.size());
    currentDisplayedScientists.clear();

    for(list<Scientist>::iterator iter = currentScientists.begin(); iter != currentScientists.end(); iter++) {
        Scientist currentScientist = Scientist(iter->getName(), iter->getGender(), iter->getYOB(), iter->getYOD());

        string searchString = ui->lineEdit_inputSciForSearch->text().toStdString();

        if(currentScientist.contains(searchString)) {
            QString name = QString::fromStdString(currentScientist.getName());
            QString gender = QString::fromStdString(currentScientist.getGender());
            QString yob = QString::fromStdString(currentScientist.yobToString());
            int yod = currentScientist.getYOD();
            QString yofdeath = "";
            if(yod != 0) {
                yofdeath = QString::fromStdString(currentScientist.yodToString());
            } else {}

            int currentRow = currentDisplayedScientists.size();

            ui->table_listOfScientists->setItem(currentRow, 0, new QTableWidgetItem(name));
            ui->table_listOfScientists->setItem(currentRow, 1, new QTableWidgetItem(gender));
            ui->table_listOfScientists->setItem(currentRow, 2, new QTableWidgetItem(yob));
            ui->table_listOfScientists->item(currentRow, 2)->setTextAlignment(Qt::AlignHCenter|Qt::AlignVCenter);
            ui->table_listOfScientists->setItem(currentRow, 3, new QTableWidgetItem(yofdeath));
            ui->table_listOfScientists->item(currentRow, 3)->setTextAlignment(Qt::AlignHCenter|Qt::AlignVCenter);

            currentDisplayedScientists.push_back(currentScientist);
        }
    }
    ui->table_listOfScientists->setRowCount(currentDisplayedScientists.size());
}

void ListAndSearchDialog::getAllComputers() {
    currentComputers = comp_actions.getAllComputers();
    displayAllComputers();
}

void ListAndSearchDialog::displayAllComputers() {
    ui->table_listOfComputers->clearContents();
    ui->table_listOfComputers->setRowCount(currentComputers.size());
    ui->table_listOfComputers->setColumnWidth(0,150);
    ui->table_listOfComputers->setColumnWidth(1,100);
    ui->table_listOfComputers->setColumnWidth(2,100);
    ui->table_listOfComputers->setColumnWidth(3,200);
    currentDisplayedComputers.clear();

    for(list<Computer>::iterator iter = currentComputers.begin(); iter != currentComputers.end(); iter++) {
        Computer currentComputer = Computer(iter->getName(), iter->getYearBuilt(), iter->getType(), iter->getWasBuilt());

        string searchString = ui->lineEdit_inputCompForSearch->text().toStdString();

        if(currentComputer.contains(searchString)) {
            QString name = QString::fromStdString(currentComputer.getName());
            int yb = currentComputer.getYearBuilt();
            QString ybuilt = "";
            if(yb != 0) {
                ybuilt = QString::fromStdString(currentComputer.ybToString());
            }
            QString type = QString::fromStdString(currentComputer.getType());
            bool wb = currentComputer.getWasBuilt();
            QString wasBuilt = "Yes";
            if(!wb) {
                wasBuilt = "No";
            }

            int currentRow = currentDisplayedComputers.size();

            ui->table_listOfComputers->setItem(currentRow, 0, new QTableWidgetItem(name));
            ui->table_listOfComputers->setItem(currentRow, 1, new QTableWidgetItem(wasBuilt));
            ui->table_listOfComputers->item(currentRow, 1)->setTextAlignment(Qt::AlignHCenter|Qt::AlignVCenter);
            ui->table_listOfComputers->setItem(currentRow, 2, new QTableWidgetItem(ybuilt));
            ui->table_listOfComputers->item(currentRow, 2)->setTextAlignment(Qt::AlignHCenter|Qt::AlignVCenter);
            ui->table_listOfComputers->setItem(currentRow, 3, new QTableWidgetItem(type));

            currentDisplayedComputers.push_back(currentComputer);
        }
    }
    ui->table_listOfComputers->setRowCount(currentDisplayedComputers.size());
}

void ListAndSearchDialog::on_lineEdit_inputSciForSearch_textChanged(const QString &arg1) {
    displayAllScientists();
}

void ListAndSearchDialog::on_lineEdit_inputCompForSearch_textChanged(const QString &arg1) {
    displayAllComputers();
}

void ListAndSearchDialog::on_table_listOfScientists_doubleClicked(const QModelIndex &index) {
    int rowNumber = index.row();
    string strToGoogle = index.sibling(rowNumber, 0).data().toString().toStdString();
    util.searchGoogle(strToGoogle);
}

void ListAndSearchDialog::on_table_listOfComputers_doubleClicked(const QModelIndex &index) {
    int rowNumber = index.row();
    string strToGoogle = index.sibling(rowNumber, 0).data().toString().toStdString();
    util.searchGoogle(strToGoogle);
}
