#ifndef DATASQLACCESS_H
#define DATASQLACCESS_H

#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <QtSql>
#include <QFile>
#include <QFileInfo>
#include <QSqlQuery>
#include <QSqlDatabase>
#include <QString>

using namespace std;

class DataSqlAccess {

    public:
        DataSqlAccess();
        void connect();
        void disconnect();
        bool databaseExists();
        QSqlDatabase getDatabaseConnection();
    private:
        QSqlDatabase database;
        QString connectionName;
};

#endif // DATASQLACCESS_H
