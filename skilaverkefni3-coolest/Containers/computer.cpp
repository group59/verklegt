#include "Containers/computer.h"

Computer::Computer(string name, int yb, string type, bool wb) {
    Name = name;
    YearBuilt = yb;
    Type = type;
    WasBuilt = wb;
}

string Computer::getName() {
    return Name;
}

string Computer::getType() {
    return Type;
}

bool Computer::getWasBuilt() {
    return WasBuilt;
}

int Computer::getYearBuilt() {
    return YearBuilt;
}

string Computer::toString() {
    return Name + " " + Type + " " + ybToString();
}

string Computer::ybToString() {
    ostringstream YB;
    YB << YearBuilt;
    return YB.str();
}

bool Computer::contains(string str) {
    if(str == "") {
        return true;
    }

    string searchStringToLower = util.stringToLower(str);

    if(util.stringToLower(this->toString()).find(searchStringToLower) != string::npos) {
        return true;
    }
    else {
        return false;
    }
}
