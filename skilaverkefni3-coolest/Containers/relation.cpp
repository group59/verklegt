#include "Containers/relation.h"

Relation::Relation(int sID, string sName, string sGender, int sYOB, int sYOD, int cID, string cName, string cType, bool cWB, int cYB) {
    SciID = sID;
    ScientistName = sName;
    ScientistGender = sGender;
    ScientistYearOfBirth = sYOB;
    ScientistYearOfDeath = sYOD;
    ComID = cID;
    ComputerName = cName;
    ComputerType = cType;
    ComputerWasBuilt = cWB;
    ComputerYearBuilt = cYB;
}

int Relation::getSciID() {
    return SciID;
}

string Relation::getScientistName() {
    return ScientistName;
}

string Relation::getScientistGender() {
    return ScientistGender;
}

int Relation::getScientistYearOfBirth() {
    return ScientistYearOfBirth;
}

int Relation::getScientistYearOfDeath() {
    return ScientistYearOfDeath;
}

string Relation::yobToString() {
    ostringstream YOB;
    YOB << ScientistYearOfBirth;
    return YOB.str();
}

string Relation::yodToString() {
    ostringstream YOD;
    YOD << ScientistYearOfDeath;
    return YOD.str();
}

int Relation::getComID() {
    return ComID;
}

string Relation::getComputerName() {
    return ComputerName;
}

string Relation::getComputerType() {
    return ComputerType;
}

bool Relation::getComputerWasBuilt() {
    return ComputerWasBuilt;
}

int Relation::getComputerYearBuilt() {
    return ComputerYearBuilt;
}

string Relation::ybToString() {
    ostringstream YB;
    YB << ComputerYearBuilt;
    return YB.str();
}

bool Relation::isItThisScientist(string str) {
    if(ScientistName == str) {
        return true;
    }
    else {
        return false;
    }
}

bool Relation::isItThisComputer(string str) {
    if(ComputerName == str) {
        return true;
    }
    else {
        return false;
    }
}
