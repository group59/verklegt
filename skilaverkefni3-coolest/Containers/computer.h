#ifndef COMPUTER_H
#define COMPUTER_H

#include <string>
#include <sstream>
#include "utilities.h"
using namespace std;

class Computer {
    private:
        Utilities util;
        string Name;
        string Type;
        bool WasBuilt;
        int YearBuilt;
    public:
        Computer(string name, int yb, string type, bool wb);
        string getName();
        string getType();
        bool getWasBuilt();
        int getYearBuilt();
        bool contains(string str);
        string toString();
        string ybToString();
};

#endif // COMPUTER_H
