#include "Containers/scientist.h"

Scientist::Scientist(string name, string gender, int yob, int yod) {
    Name = name;
    Gender = gender;
    YearOfBirth = yob;
    YearOfDeath = yod;
}

string Scientist::getName() {
    return Name;
}

string Scientist::getGender() {
    return Gender;
}

int Scientist::getYOB() {
    return YearOfBirth;
}

int Scientist::getYOD() {
    return YearOfDeath;
}

string Scientist::toString() {
    return Name + " " + Gender + " " + yobToString() + " " + yodToString();
}

string Scientist::yobToString() {
    ostringstream YOB;
    YOB << YearOfBirth;
    return YOB.str();
}

string Scientist::yodToString() {
    ostringstream YOD;
    YOD << YearOfDeath;
    return YOD.str();
}

bool Scientist::contains(string str) {
    if(str == "") {
        return true;
    }

    string searchStringToLower = util.stringToLower(str);

    if(util.stringToLower(this->toString()).find(searchStringToLower) != string::npos) {
        return true;
    }
    else {
        return false;
    }
}
