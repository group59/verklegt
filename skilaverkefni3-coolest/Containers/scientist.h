#ifndef SCIENTIST_H
#define SCIENTIST_H

#include <string>
#include <sstream>
#include "utilities.h"
using namespace std;

// A class that defines and keeps info about each person
class Scientist {
    private:
        Utilities util;
        string Name;
        string Gender;
        int YearOfBirth;
        int YearOfDeath;
    public:
        Scientist(string name, string gender, int yob, int yod);
        string getName();
        string getGender();
        int getYOB();
        int getYOD();
        bool contains(string str);
        string toString();
        string yobToString();
        string yodToString();
};

#endif // SCIENTIST_H
