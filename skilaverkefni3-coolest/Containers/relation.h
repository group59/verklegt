#ifndef RELATION_H
#define RELATION_H

#include <string>
#include <sstream>
using namespace std;

class Relation {
    private:
        int SciID;
        string ScientistName;
        string ScientistGender;
        int ScientistYearOfBirth;
        int ScientistYearOfDeath;
        int ComID;
        string ComputerName;
        string ComputerType;
        bool ComputerWasBuilt;
        int ComputerYearBuilt;
    public:
        Relation(int sID, string sName, string sGender, int sYOB, int sYOD, int cID, string cName, string cType, bool cWB, int cYB);
        int getSciID();
        string getScientistName();
        string getScientistGender();
        int getScientistYearOfBirth();
        int getScientistYearOfDeath();
        string yobToString();
        string yodToString();
        int getComID();
        string getComputerName();
        string getComputerType();
        bool getComputerWasBuilt();
        int getComputerYearBuilt();
        string ybToString();
        bool isItThisScientist(string str);
        bool isItThisComputer(string str);
};

#endif // RELATION_H
