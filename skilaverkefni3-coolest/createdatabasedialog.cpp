#include "createdatabasedialog.h"
#include "ui_createdatabasedialog.h"

CreateDatabaseDialog::CreateDatabaseDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CreateDatabaseDialog) {

    ui->setupUi(this);
}

CreateDatabaseDialog::~CreateDatabaseDialog() {
    delete ui;
}

bool CreateDatabaseDialog::on_buttonBox_accepted() {
    return true;
}

bool CreateDatabaseDialog::on_buttonBox_rejected() {
    return false;
}
